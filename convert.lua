function show_tilesets(d)
	local k, l
	print("tiles = tilesets {");
	print("\ttiles = { ");
	for k,l in ipairs(d.tilesets) do
		print("\t\t{ file = '"..l.image.."'"..", id = "..tostring(l.firstgid).."},")
	end
	print("\t};");
	print("};");
end
function show_objects(l)
	local x, y, k, o
	for k, o in ipairs(l.objects) do
		print("\t\ttobj {")
		print("\t\t\tnam = '"..o.name.."';");
		print("\t\t\tx = "..tostring(o.x / 12)..";")
		print("\t\t\ty = "..tostring(o.y / 12 - 1)..";")
		print("\t\t\ttile = "..tostring(o.gid)..";")
		print("\t\t},")
	end
end

function show_layer(l)
	local x, y
	for y = 1, l.height do
		local line=''
		for x = 1,l.width do
			line = line..tostring(l.data[(y - 1)* l.width + x])..","
		end
		print("\t\t"..line)
	end
end

function make_room(d, name)
	local k,l
	print (name.." = troom {");
	for k,l in pairs(d.layers) do
		if l.type == "tilelayer" then
			print("\t"..l.name.." = {")
			show_layer(l)
			print("\t};")
		elseif l.type == "objectgroup" then
			print("\tobj = {") 
			show_objects(l)
			print("\t};")
		end
	end
	print("}")
end
a = { ... }
if #a ~= 2 then
	print "Usage: convert map.lua room|tilesets"
	return
end
local f = assert(loadfile(a[1]))
data = f()
if a[2] == 'tilesets' then
	show_tilesets(data)
else
	make_room(data, a[2])
end
