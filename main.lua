--$Name: Woodcutter$
--$Name(ru): Дровосек$
--$Name(es): Leñador$

--$Version: 1.3$
--$Author: Peter Kosyh$

instead_version "1.8.0"

require "sprites"
require "hideinv"
require "click"
require "kbd"
require "theme"
require "nouse"
require "nolife"
require "para"
require "dash"
require "quotes"
require "snapshots"
require "xact"
require "timer"

stead.mouse_filter(0)

game.active = {}

W = 9
H = 9
TW = 48
TH = 48
game.gui.hinv_delim = ''
hook_keys("up", "down", "left", "right")

global { use_mode = null, move_mode = true };
move_dir = 0;
iface.title = function() end
game.kbd = function(s, down, key)
	if not here().troom_type or timer:get() ~= 0 or disabled(hero) then
		return
	end
	if not down then
		if key == move_dir then
			move_dir = 0
		end
		return
	end
	local x, y = stead.mouse_pos()
	if x >= tonumber(theme.get('inv.x')) and y >= tonumber(theme.get('inv.y')) and
		x < tonumber(theme.get('inv.x')) +  tonumber(theme.get('inv.w')) and
		y < tonumber(theme.get('inv.y')) +  tonumber(theme.get('inv.h')) then
		return
	end
	move_dir = key
	if move_dir == "left" then
		return me():move(-1, 0);
	elseif move_dir == "right" then
		return me():move(1, 0);
	elseif move_dir == "up" then
		return me():move(0, -1);
	elseif move_dir == "down" then
		return me():move(0, 1);
	end
end

function isFar(x, y)
	if x ~= me().x and y ~= me().y then
		return true
	end
	if math.abs(x - me().x) > 1 or math.abs(y - me().y) > 1 then
		return true
	end
	return false
end

game.click = function(s, x, y)
	local xx, yy
	local i, o
	local w, h

	if not here().troom_type or timer:get() ~= 0 or disabled(hero) then
		return
	end
	xx = math.floor(x / TW)
	yy = math.floor(y / TH)

	local dx, dy
--	dx = x - me().x * TW - TW/2
--	dy = y - me().y * TH - TH/2

	dx = xx - me().x
	dy = yy - me().y

	if dx == 0 and dy == 0 then
		if use_mode ~= null then
			local r,v = me():use(use_mode, hero)
			use_mode = null
			return r, v
		end
		return stead.call(hero, 'inv')
	end
	if math.abs(dx) > math.abs(dy) then
		return me():move(dx, 0);
	else
		return me():move(0, dy);
	end
end

function isWall(x, y, wh)
	if not wh then wh = here() end
	if not wh.wall then return end
	if y >= H or x >= W or x < 0 or y < 0 then
		return false
	end
	if wh.wall[y * W + x + 1] ~= 0 then
		return wh.wall[y * W + x + 1]
	end
	return false
end


local function get_objects(x, y, wh)
	local i, o, oo, w, h
	local oo = {}
	for i,o in opairs(objs(wh)) do
		o = stead.ref(o);
		w = 1; h = 1
		if o.w then w = o.w end
		if o.h then h = o.h end
		if isObject(o) and not isDisabled(o) then
			if x >= o.x and y >= o.y and x < o.x + w and y < o.y + h then
				stead.table.insert(oo, o)
				if o.obstacle then
					oo.obstacle = true
				end
			end
		end
	end
	return oo
end

function isObstacle(x, y, wh)
	if isWall(x, y, wh) then
		return true
	end
	local oo = get_objects(x, y, wh)
	while #oo > 0 and oo[1].hidden do
		table.remove(oo, 1)
	end
	return oo.obstacle
end

function isSomething(x, y, wh)
	if isWall(x, y, wh) then
		return true
	end
	local oo = get_objects(x, y, wh)
	while #oo > 0 and oo[1].hidden do
		table.remove(oo, 1)
	end
	return #oo > 0 or ((wh == nil or here() == wh) and me().x == x and me().y == y)
end

local function do_border(wh)
	local w, x, y
	if type(wh) == 'table' and not isRoom(wh) then
		w = wh[1]
		x = wh[2]
		y = wh[3]
		walk(w)
		me().x = x;
		me().y = y
	else
		walk(wh)
	end
end

pl = player {
	nam = 'player';
	var { x = 4; y = 8; };
	tile = 8;
	move = function(s, dx, dy)
		local x, y
		x = s.x
		y = s.y
		if dx > 0 then
			x = x + 1
		elseif dx < 0 then
			x = x - 1
		end
		if dy > 0 then
			y = y + 1
		elseif dy < 0 then
			y = y - 1
		end
		ACT_X = x;
		ACT_Y = y;
		local i, o
		local oo = get_objects(x, y)
		local v = true
		local obstacle
		for i, o in ipairs(oo) do
			if use_mode ~= null then
				if not o.skip then
					local vv = me():use(use_mode, o)
					v = stead.par(stead.space_delim, v, vv);
				end
			else
				v = stead.par(stead.space_delim, v, me():action(o))
			end
			if o.stop or (use_mode ~= null and not o.skip) then
				break
			end
		end
--		if #oo > 0 then
			if use_mode ~= null then
				obstacle = true
			end
			use_mode = null
--		end
		if not v then v = true end
		if oo.obstacle or obstacle then
			return v
		end
		if player_moved() or isWall(x, y) then
			use_mode = null
			return v
		end
		s.x = x
		s.y = y
		if s.x < 0 then
			if here().west then
				do_border(here().west)
			end
		elseif s.x >= W then
			if here().east then
				do_border(here().east)
			end
		elseif s.y < 0 then
			if here().north then
				do_border(here().north)
			end
		elseif s.y >= H then
			if here().south then
				do_border(here().south)
			end
		end
		if s.x < 0 then
			s.x = 0
		end
		if s.x >= W then
			s.x = W - 1
		end
		if s.y < 0 then
			s.y = 0
		end
		if s.y >= H then
			s.y = H - 1
		end
		return v
	end
}
reload = xact("reload", function(s)
	return restore_snapshot()
end)

noreload = xact("noreload", function(s)
	return true
end)

hero = obj {
	nam = 'hero';
	menu_type = true;
	disp = false;
	tile = 10;
}

restart = obj {
	nam = '^'..txtr(img('gfx/no.png'));
	menu_type = true;
	tile = 10;
	inv = function(s)
		pn (txtc [[¿Empezar de nuevo?]])
		pn (txtc [[{reload|Sí}  |  {noreload|No}]])
	end
}

take 'hero'
me().obj[10000] =  'restart'


blank = sprite.blank(W * TW, H * TW);
window = sprite.box(W * TW, H * TH);
game.pic = function(s)
	if here().troom_type then
		return window
	end
	if isDialog(here()) then
		sprite.fill(window, 'black')
		tiles:draw(here().tile, window, 3 * TW, 4 * TH)
		tiles:draw(hero.tile, window, 5 * TW, 4 * TH)
		return window
	end

	if here() == gameover then
		sprite.fill(window, 'black')
		tiles:draw(here().tile, window, 4 * TW, 4 * TH)
		return window
	end
end

tilesets = function(v)
	local k,t

	local w, h

	v.nam = 'tiles';

	if not v.w then v.w = TW end
	if not v.h then v.h = TW end

	for k,t in ipairs(v.tiles) do
		local n
		t.spr = sprite.load(t.file);
		if t.scale then n = sprite.scale(t.spr, t.scale, t.scale, false); sprite.free(t.spr); t.spr = n;  end
		w, h = sprite.size(t.spr)
		t.w = math.floor(w / v.w);
		t.h = math.floor(h / v.h);
		t.nr = t.w * t.h
	end

	v.spr = function(s, id)
		local k, v, x, y
		for k,v in ipairs(s.tiles) do
			if id >= v.id and id - v.id < v.nr then
				id = id - v.id
				x, y = id % v.w, math.floor(id / v.w)
				return v.spr, x, y
			end
		end
		return nil
	end;

	v.copy = function(s, gid, to, tx, ty)
		local x, y
		local spr
		spr, x, y = s:spr(gid)
		sprite.copy(spr, x * s.w, y * s.h, s.w, s.h, to, tx, ty);
	end
	v.draw = function(s, gid, to, tx, ty)
		local x, y
		local spr
		spr, x, y = s:spr(gid)
		sprite.copy(spr, x * s.w, y * s.h, s.w, s.h, to, tx, ty);
	end
	v.compose = function(s, gid, to, tx, ty)
		local x, y
		local spr
		spr, x, y = s:spr(gid)
		sprite.compose(spr, x * s.w, y * s.h, s.w, s.h, to, tx, ty);
	end
	return obj(v)
end

tobj = function(v)
	v.menu_type = true
	if not v.nam then v.nam = true end
	v.disp = function(s)
		if not s.spr then
			s.spr = sprite.blank(TW, TH)
			tiles:compose(s.tile, s.spr, 0, 0);
		end
		if not s.spr_active then
			s.spr_active = sprite.box(TW, TH, 'white')
			b = sprite.blank(TW - 2, TH - 2)
			sprite.copy(b, s.spr_active, 1, 1)
			sprite.free(b)
			tiles:compose(s.tile, s.spr_active, 0, 0);
		end
		if use_mode == s then
			return img(s.spr_active)
		end
		return img(s.spr)
	end;
	v.oinv = v.inv

	v.inv = function(s)
		if not here().troom_type or timer:get() ~= 0 or disabled(hero) then
			return
		end
		if use_mode == s then
			use_mode = null
			return stead.call(s, 'oinv')
		end
		if use_mode == null then
			use_mode = s
			return nil, true
		end
		local r,v = me():use(use_mode, s);
		use_mode = null
		if not r and not v then return nil, true end
		return r, v
	end;

	v.look = function(self)
		local i, vv, o
		if isDisabled(self) then
			return
		end
		local v = stead.call(self,'dsc');
		v = self:xref(v);
		if self.tile then
			local t = tiles
			local dx, dy
			dx = 0
			dy = 0;
			if self.dx then dx = self.dx end
			if self.dy then dy = self.dy end
			t:compose(self.tile, window, self.x * TW + dx, self.y * TH + dy);
		end
		for i,o in opairs(self.obj) do
			o = stead.ref(o);
			if isObject(o) then
				vv = o:look();
				v = stead.par(stead.space_delim, v, vv);
			end
		end
		return v;
	end

	v.breakable = function(s, txt, item)
		s.item = item
		s.used = function(s, w)
			if w == topor then
				p(txt)
				s:disable()
				if not s.item then
					-- p [[Он оказался пустым.]]
					put (new (string.format("garbage(%d, %d)", s.x, s.y)))
					return
				end
				put(s.item)
				stead.ref(s.item).x = s.x
				stead.ref(s.item).y = s.y
			end
		end;
		return s
	end
	v.movable = function(s)
		s.act = function(s)
			local dx = s.x - me().x
			local dy = s.y - me().y
			local x = s.x + dx
			local y = s.y + dy
			if isWall(x, y) or x < 0 or y < 0 or x >= W or y >=H then
				return stead.call(s, 'noact')
			end
			local i,o, w, h
			for i,o in opairs(objs()) do
				w = 1; h = 1
				if o.w then w = o.w end
				if o.h then h = o.h end
				if isObject(o) and not isDisabled(o) then
					if x >= o.x and y >= o.y and x < o.x + w and y < o.y + h then
						if o.obstacle then
							return stead.call(s, 'noact')
						end
					end
				end
			end
			if s.weight and not s._weight then
				s._weight = s.weight
			end
			if s._weight and s._weight ~= 0 then
				s._weight = s._weight - 1
				return true
			end
			s._weight = nil
			s.x = x
			s.y = y
			me().x = me().x + dx
			me().y = me().y + dy
			return true
		end
		return s
	end
	return menu(v)
end

troom = function(v)
	if not v.nam then v.nam = true end
	v.troom_type = true;
	v.odsc = v.dsc
	v._tick = 0;
	v._active = 0;
	v.dsc = function(s)
		if s.refer then
			s = stead.ref(s.refer)
		end

		if s.nodsc then
			return stead.call(s, 'odsc')
		end

		if player_moved() then
			s._tick = 1
		elseif stead.state then
			s._tick = s._tick + 1
		end
		if s._tick % 8 == 0 then
--			if s._tick == 0 and not s._dsc then
--				s._dsc = stead.call(s, 'odsc');
--				return s._dsc;
--			end
			s._tick = s._tick + 1
			local n = s._tick - 1
			local an = 0
			if s.active then
				an = #s.active
				if s.noactive then
					an = 0
				end
			end
			if (s._active < an or #game.active > 0 or (s.lifes and #s.lifes > 0)) and rnd(100) < 35 then
				if (#game.active > 0 or (s.lifes and #s.lifes > 0)) and (s._active >= an or rnd(100) < 35) then
					if (rnd(100) < 50 and #game.active > 0) or #s.lifes == 0 then
						s._dsc = game.active[rnd(#game.active)]
					else
						s._dsc = s.lifes[rnd(#s.lifes)]
					end
				else
					s._active = s._active + 1
					s._dsc = s.active[s._active]
				end
			else
				s._dsc = stead.call(s, 'dsc2')
			end
		end
		if not s._dsc then
			s._dsc = stead.call(s, 'odsc');
		end
		return s._dsc
	end;
	v.ini = function(s, load)
		if load and here() == v then
			v:look()
		end
	end
	v.enter = function(s, f)
		local ss = s
		if s.refer then ss = stead.ref(s.refer) end
		if not ss.noreinit and not ss.nodsc then
			ss._tick = 0
			ss._active = 0
			ss._dsc = stead.call(ss, 'odsc')
		end

		if s == map11e or s == map11w or s == map11ne or s == map11se or s == map11nw or s == map11sw or s == map12 or s == gameover then
			return
		end

		if (f == map11se and s == map10) and ((not have 'heart') or (skeleton.y == 2)) then
			return
		end

		if f == map12 then
			return
		end

		make_snapshot()
	end;
	v.look = function(s)
		-- clear all
		sprite.copy(blank, window, 0, 0);
		-- bg
		local x, y
		local t = tiles;
		for y = 0, H - 1 do
			for x = 0, W - 1 do
				local c = s.bg[y * W + x + 1];
				if c ~= 0 then
					t:copy(c, window, x * t.w, y * t.h);
				end
			end
		end
		-- ground
		if s.ground then
			for y = 0, H - 1 do
				for x = 0, W - 1 do
					local c = s.ground[y * W + x + 1];
					if c ~= 0 then
						t:compose(c, window, x * t.w, y * t.h);
					end
				end
			end
		end
		-- walls
		if s.wall then
			for y = 0, H - 1 do
				for x = 0, W - 1 do
					local c = s.wall[y * W + x + 1];
					if c ~= 0 then
						t:compose(c, window, x * t.w, y * t.h);
					end
				end
			end
		end
		-- objects on ground
		local i,o
		local vv = ''
		for i,o in opairs(s.obj) do
			o = stead.ref(o);
			if isObject(o) and not o.float then
				vv = stead.par(stead.space_delim, vv, o:look());
			end
		end
		-- hero
		if not disabled(hero) then
			tiles:compose(hero.tile, window, me().x * TW, me().y * TH);
		end
		-- float objects
		for i,o in opairs(s.obj) do
			o = stead.ref(o);
			if isObject(o) and o.float then
				vv = stead.par(stead.space_delim, vv, o:look());
			end
		end
		if vv == '' then return end
		return vv
	end
	return room(v)
end

topor = tobj {
	tile = 13;
	inv = 'Mi hacha. ¡Gran herramienta! ';
	nouse = 'Es una pena, pero un hacha no va a ayudar.';
	var { x = 5;
	y = 4; };
	tak = '¡Mi hacha!';
};

block = function(w)
	local r = room {
		nam = true;
		enter = function(s)
			p(w)
			return false
		end;
	}
	return r
end

walkpos = function(x, y, w)
	if (w) then
		walk(w)
	end
	me().x = x
	me().y = y
end

dofile "tiles.lua"
dofile "map.lua"

take 'topor'

main = room {
	nam = '...';
	hideinv = true;
	entered = function()
		theme.win.geom(200, 164, 400, 320);
	end;
	left = function()
		theme.win.reset();
		theme.gfx.reset();
		theme.gfx.bg('gfx/bg.png')
		set_music 'mus/4.ogg'
	end;
	dsc = function(s)
		if theme.name() ~= '.' then
			return [[Este juego sólo puede funcionar con su propio tema. Por favor,
			       activa "temas de juego personalizados: Sí" en los ajustes de INSTEAD.]];
		end
		p (txtb [[¡ATENCIÓN!^]], [[¡¡¡Este juego puede arruinar tus nervios y tu cerebro!!!^
		Si prefieres los juegos casuales, no pierdas el tiempo.]])
	end;
	obj = { vway('next', '{Continuar}', 'prolog') };
}
prolog = room {
	nam = 'Leñador';
--	scaledpic = 'gfx/title.png';
	pic = 'gfx/title4x.png';
	hideinv = true;
	dsc = [[Me llamo Ron, y soy leñador. El malvado hechicero Daktro ha secuestrado y encarcelado a mi esposa Arabella en una alta torre negra mientras yo trabajaba en el bosque.
	Tengo mi hacha y voy a ir hacia la torre negra.
	^¡Maldito Dactro, voy a por ti!]];
	obj = { vway('next', '{Comenzar}', 'game_start') };
}

game.forcedsc = true
-- dofile "scaler.lua" -- for debug only
