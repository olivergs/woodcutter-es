local function ltime(n)
	local t = n
	local l = t % 10
	if t % 100 <= 20 then
		l = t % 100;
	end
--	if t >=5 and t <= 20 then
--		return t.." ходов";
--	end
	if l == 0 or l >= 5 then
		return t.." movimientos";
	end
	if l == 1 then
		return t.." movimientos";
	end
	return t.." movimientos";
end

happyend2 = troom {
	nam = 'El fin';
	nodsc = true;
	entered = function(s)
		s._time = time();
		s._stamp = os.time(os.date("*t"));

		lifeoff(cat)
		lifeoff(arabella)
		remove(cat, me())
		me().x = 4
		me().y = 6
	end;
	dsc = function(s)
		local f = exist 'fire'

		pn ([[¡Felicidades! ¡Completaste este pequeño juego en ]]..ltime(s._time)..[[!]]);
		if not disabled(f) then
			local st = s._stamp % 65536
			local t = s._time % 65536;
			local crc = st * t % 123121 % 65536;
			local c = txtb(string.format("%04X%04X%04X", st, t, crc))
			pn (txtem ([[¡Escribe un mensaje en el foro INSTEAD, que contiene el código: ]]..c..[[ y tu nombre se agregará a la lista de héroes!]]))
		end
		pn [[Autor del código : Петр Косых^
		Dibujos: Петр Косых, Александр Соборов^
		Música: Александр Соборов, Mister Beep^]];
		pn (txtu ([[Lista]]..txtnb " "..[[de]]..txtnb " "..[[héroes:]]))
		pn ('_'..txttab("0%")..[[0...]]..txttab "50%"..[[excelenter]])
		pn ('_'..txttab("0%")..[[1...]]..txttab "50%"..[[zaynyatyi]])
		pn ('_'..txttab("0%")..[[2...]]..txttab "50%"..[[vvb]])
		pn ('_'..txttab("0%")..[[3...]]..txttab "50%"..[[j-maks]])
		pn ('_'..txttab("0%")..[[4...]]..txttab "50%"..[[Daniil]])
		pn()
		pn (txtr [[https://instead.hugeping.ru^
		2012]]);
	end;
	bg = {
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
	};
	obj = {
		tobj {
			nam = 'arabella';
			obstacle = true;
			x = 2;
			y = 4;
			tile = 55;
			act = [[-- ¡Mi héroe!]];
			used = function(s, w)
				return stead.call(arabella, 'used', w)
			end
		},
		tobj {
			nam = 'heart';
			obstacle = true;
			x = 4;
			y = 4;
			act = '...';
			used = function(s, w)
				if w == fakel then
					local f = exist 'fire'
					f:enable()
					s:disable()
					set_music 'mus/tpr.ogg'
					return true
				end
				if w == topor or w == knife then
					s:disable()
					put(blood)
					blood.x = 4
					blood.y = 4
					return true
				end
			end;
			tile = 53;
		},
		tobj {
			nam = 'fire';
			tile = 47;
			x = 4;
			y = 4;
			obstacle = true;
			act = "!!!";
			float = true;
		}:disable();
		tobj {
			nam = 'cat';
			obstacle = true;
			x = 6;
			y = 4;
			act = [[ -- -- Me quedaré contigo.]];
			tile = 57;
			used = function(s, w)
				return stead.call(cat, 'used', w)
			end
		},
	};
}
