function water(x, y, w, h)
	return	tobj {
			nam = 'water';
			obstacle = true;
			x = x;
			y = y;
			w = w;
			h = h;
			used = true;
		};
end

rope = tobj {
	nam = 'cuerda';
	tile = 16;
	inv = "Una cuerda larga y fuerte. Forma parte del aparejo del barco.";
	nouse = "¿Por qué hay una cuerda?";
	use = function(s, w)
		if nameof(w) == 'kolodec' then
			return "¿A qué puedo amarrar el extremo de la cuerda?"
		end
		if nameof(w) == 'tree1' then
			if w.y == 7 then
				p "Até un extremo de la cuerda a un tronco y dejé caer el otro extremo en el pozo."
			else
				return "Está demasiado lejos del pozo, no tiene sentido hacerlo."
			end
			remove(s, me())
		end
	end
}

brevno = function(s)
	local dx = s.x - me().x
	local dy = s.y - me().y
	local x = s.x + dx
	local y = s.y + dy
	local wall = isWall(x, y)
	if s._water then
		me().x = s.x
		me().y = s.y
		return true
	end
	if isWall(x, y) or x >= W or y >= H or x < 0 or y < 0 then
		return
	end
	local i,o, w, h
	for i,o in opairs(objs()) do
	w = 1; h = 1
	if o.w then w = o.w end
		if o.h then h = o.h end
		if isObject(o) and not isDisabled(o) then
			if x >= o.x and y >= o.y and x < o.x + w and y < o.y + h then
				if nameof(o) == 'water' then
					s._water = true
					s.obstacle = false
					break
				end
				if o.obstacle or o.brevno then
					return
				end
			end
		end
	end
	s.x = x
	s.y = y
	me().x = me().x + dx
	me().y = me().y + dy
	return true
end
game_start = troom {
	nam = 'Cerca de la torre';
	dsc = [[Y aquí estoy, en la Península Negra. Es un lugar sombrío. Una torre siniestra se eleva sobre un acantilado.
		Los graznidos de las gaviotas suenan como un llanto en medio del ruido de las olas. Los rayos del sol poniente proyectan
    sombras de ramas de árboles nudosos.]];
	lifes = {[[De repente, un viento penetrante sopló desde el mar.]],
		[[El cielo se ha vuelto negro. Las nubes lo cubrieron.]]};
	active = { [[Creo que veo luz en el último piso de la torre. Arabella, ¿estás ahí?]],
		[[¿Cómo voy a entrar en la torre?]]};
	dsc2 = [[La torre negra se eleva sobre el acantilado.]];
	bg = {
		11,7,0,0,0,0,0,0,0,
		0,7,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,4,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
	};
	ground = {
		11,0,0,0,0,0,0,11,11,
		11,0,0,0,0,0,0,11,11,
		11,11,0,0,0,0,0,11,11,
		11,0,0,0,0,0,0,0,0,
		11,0,0,0,0,0,0,0,0,
		11,0,0,0,12,0,0,0,0,
		11,0,0,0,12,0,0,0,0,
		11,0,0,0,12,0,0,0,0,
		11,0,0,0,12,0,0,0,0,
	};
	wall = {
		0,0,3,1,1,1,2,0,0,
		0,0,3,1,1,1,2,0,0,
		0,0,3,1,1,1,2,0,0,
		0,0,3,1,1,1,2,0,0,
		0,0,3,1,0,1,2,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	obj = {
		tobj {
			nam = 'door';
			x = 4;
			y = 4;
			tile = 5;
			obstacle = true;
			act = "La puerta de la torre del brujo es sólida, con gruesas barras de metal y un gran candado.";
			used = function(s, w)
				if w == topor then
					return "Me temo que dañará mi hacha."
				end
			end
		},
		tobj {
			nam = 'kolodec';
			x = 8;
			y = 8;
			tile = 9;
			obstacle = true;
			used = function(s, w)
				if w == topor then
					p [[El hacha cayó al fondo del pozo.]]
					remove(topor, me())
					put(topor, 'map2')
				end
			end;
			act = function(s)
				if taken 'rope' and not have 'rope' then
					p [[Bajé a un pozo seco.]]
					walk 'map2'
					return
				end
				p [[Un pozo seco, sin agua. Y las cadenas con un cubo también vacío.]];
			end
		},
		tobj {
			nam = 'tree1';
			var {
			x = 8;
			y = 6; };
			var { tile = 8; brevno =  false;  obstacle = true;};
			used = function(s, w)
				if w == topor then
					if s.brevno then
						return "Ahora no hay tiempo para hacer leña."
					end
					s.brevno = true
--					s.obstacle = false
					s.tile = 14
					pr "¡Hecho!"
				end
			end;
			act = function(s)
				if s.brevno then
					return brevno(s)
				end
				return "Un árbol seco. Sin una sola hoja."
			end
		},
		tobj {
			nam = 'tree2';

			var { x = 6;
			y = 5; };
			var { tile = 8; brevno =  false; obstacle = true;};
			used = function(s, w)
				if w == topor then
					if s.brevno then
						return "Ahora no hay tiempo para hacer leña."
					end
					s.brevno = true
--					s.obstacle = false
					s.tile = 14
					p "¡Hecho!"
				end
			end;
			act = function(s)
				if s.brevno then
					return brevno(s)
				end
				return "Un árbol seco. Sin una sola hoja."
			end
		},
		tobj {
			nam = 'boat';
			x = 0;
			y = 0;
			obstacle = true;
			tile = 15;
			used = function(s, w)
				if w == topor then
					p "¡Toma eso, maldito barco!"
					s:disable()
				end
			end;
			act = function(s)
				if not taken(rope) then
					p "Había una cuerda en el fondo del barco. La tomé."
					take (rope)
				else
					p "Un barco que hace aguas. ¿De dónde viene?"
				end
			end;
		},
		tobj {
			nam = 'win2';
			x = 3;
			y = 2;
			tile = 6;
		},
		tobj {
			nam = 'win1';
			x = 5;
			y = 0;
			tile = 6;
		},
		water(1, 2);
		water(0, 0, 1, 9);
		water(7, 0, 2, 3);
	};
}
