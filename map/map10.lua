global { heart_know = false };
shovel = tobj {
	nam = 'shovel';
	tile = 49;
	var { x =0, y = 0 };
	inv = [[Está un poco oxidada.]];
	nouse = [[La pala es inútil aquí.]];
	act = code [[ take(self); p "Cogí una pala."]];
}

notes = tobj {
	nam = 'notes';
	tile = 50;
	var { x =0, y = 0, readed = false; };
	nouse = [[Cogí una pala.]];
	use = function(s, w)
		if nameof(w) == 'fireplace' or nameof(w) == 'fire' then
			remove(s, me())
			p [[¡Arde, el mal!]]
			return
		end
	end;
	used = function(s, w)
		if w == fakel then
			remove(s, me())
			p [[¡Arde, el mal!]]
			return
		end
	end;
	inv = function(s)
		s.readed = true
		p [[Algunos esquemas y diagramas... En la parte posterior de una de las páginas quemadas leí: ^
		"... me senté junto a la chimenea durante mucho tiempo, estaba temblando... ¡Aún así, hoy saqué mi corazón de mi pecho!
		Luego pasó la emoción, recordé las palabras del poema del gran al Kabib una y otra vez, repitiéndolas como un hechizo...^
		Bueno, mi corazón está a salvo, y mientras descanse bajo tierra, ni yo ni él -- ¡no nos molestamos el uno al otro!^
		Extrañamente, el deseo de poder me abruma.
		Pensé que al esconder mi corazón me volvería desapasionado, pero parece que no hay camino intermedio ...^
		¡Solo hay dos polos! Hoy, después de haber creado este antiguo hechizo, cortando el corazón de mi pecho,
		he pasado al Polo al que he estado gravitando toda mi vida... Y ahora, he encontrado la inmortalidad..."]];
	end;
}

map10 = troom {
	nam = 'octavo piso';
	noreinit = true;
	var { noactive = false };
	dsc = function(s)
		p [[¡Estamos subiendo más y más alto! Al mirar a mi alrededor, me di cuenta de inmediato
		  de que este piso sirve como biblioteca para Dactro.
			Todas las paredes están forradas de librerías. ¡Maldita sea!
			Hay un esqueleto en la esquina. Es cierto que todavía no nos ve.^
			Pero el paso arriba parece libre.]];
	end;
	dsc2 = [[Este piso está ocupada por una biblioteca. El pasillo de arriba está ubicado en la esquina.]];
	lifes = { [[-- Con tantos libros, no es de extrañar que Dactro esté loco.^ -- ¿Está loco?^ -- El mal siempre es una locura, Ron.]],
		[[Puedo escuchar la leña crepitando siniestramente en la chimenea.]],
		[[Creo que oigo un ruido arriba.]],
		};
	active = { [[ -- Escucha, Rata, estaba pensando que esto no es probable, pero tú no eres mi esposa Arabella, ¿verdad?^
		-- !!!???^
		-- Bueno, tú no eras mi esposa Arabella, ¿verdad?^
		-- !!???^
		-- Bueno, estabas hablando de tu nombre...^
		-- ¡¡¡Puaj!!! ¿Cómo pudiste pensar eso? ¡No soy una mujer! ¿O crees que la magia puede convertir a una mujer en un hombre?
		   Definitivamente, la torre tiene un mal efecto en tu psique. ¡¡¡Pensar tal cosa!!! ¡¡¡Puaj!!!^
		-- Vale, lo siento...]],
		[[ -- ¿Cuánto tiempo crees que aún nos queda por recorrer?^
		-- Mi intuición me dice que Dactro está muy cerca.]],
		},
	entered = function(s, f)
		if f ~= map11se then
			if f == map12 then
				me().x = 7
				me().y = 7
			else
				me().x = 1
				me().y = 1
			end
		end
		skeleton.fooled = false
		skeleton.started = false
		if f == map9 then
			skeleton.x = 7
			skeleton.y = 1
		end
		lifeon(skeleton)
		set_music 'mus/5.ogg'
	end;
	left = function(s)
		lifeoff(skeleton)
	end;

	bg = {
		0,0,0,0,0,0,0,0,0,
		0,18,18,18,18,18,18,46,0,
		0,18,46,18,18,18,18,18,4,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,46,46,0,
		0,18,18,46,18,18,18,18,0,
		0,18,18,18,18,46,18,18,0,
		0,46,18,18,18,18,18,46,0,
		0,0,0,0,0,0,0,0,0,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,17,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		1,1,1,1,1,1,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,0,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,1,1,1,1,1,1,1,1,
	};
	obj = {
		tobj {
			nam = 'out';
			x = 7;
			y = 7;
			tile = 17;
			obstacle = true;
			act = function(s)
				if not taken 'heart' then
					p [[¡El camino estaba libre! Empecé a subir los escalones cuando escuché un sonido.
					¡Miré hacia arriba y vi a Dactro!
					El vil hechicero agitó sus manos y una bola de fuego voló en mi dirección.
					Huir era inútil...]];
					gameover.tile = 47;
					walkin 'gameover'
					return
				end
				walk 'map12'
			end
		},
		tobj {
			nam = 'table';
			var {
				x = 4;
				y = 4;
			};
			obstacle = true;
			tile = 43;
		}:movable():breakable('¡Rompí la mesa!'),
		tobj {
			nam = 'chair';
			var {
				x = 5;
				y = 4;
			};
			obstacle = true;
			tile = 37;
		}:movable():breakable('Rompí la silla.'),
		books(3, 1, [[Algún tipo de metafísica...]]);
		books(5, 1, [[Um, astrología...]]);
		books(1, 3, [[Árabe... No sé leer en árabe...]]);
		books(1, 4, [["Gran Enciclopedia Insular".]]);
		books(1, 5, [[Filosofía...]]);
		books(7, 3, [[Algunos libros ilustrados.]]);
		books(7, 4, function(s)
			if notes.readed then
				heart_know = true;
				p [[Mirando el estante con libros, noté un pequeño volumen, en el lomo del cual estaba impreso "Al Kabib, Océano de Lágrimas.". ^
				Abrí el volumen en el índice... Uno de los poemas se llamaba "Epitafio":^]]
				p [["Cuando muera, esconde mi cuerpo,^
				Bajo el pino alto ...^
				Junto al río que siempre llora ^
				Tranquilo, pero tan amargo...^
				Junto a una piedra, al lado del camino^
				Cava un agujero...^
				¡Deja mis restos allí!^
				Y llena mi herida...
				"^]]


				p [[Hum... Pésimo poema... O una traducción fallida...]]
				return
			end
			p [[Algunos libros...]]
		end
		);
		books(7, 5, [[Libros de historia, por lo que puedo entender...]]);
		books(3, 7, [[Un lenguaje incomprensible para mí.]]);
		books(4, 7, [[Libros muy antiguos.]]);
		books(5, 7, [[Ciertamente, en estos libros está escrita algún tipo de maldad.]]);
		'skeleton',
		tobj {
			nam = 'teleport';
			x = 8;
			y = 2;
			skip = true;
			obstacle = true;
			tile = 30;
			act = function(s)
				if disabled(exist 'wdoor') then
					lifeoff(skeleton);
					walk 'map11w'
				end
			end;
		}:disable(),
		wooden_door(8, 2, 1):disable();
		tobj {
			nam = 'block';
			x = 8;
			y = 2;
			skip = true;
			obstacle = true;
			tile = 3;
		},

		tobj {
			nam = 'hob2';
			x = 8;
			y = 2;
			tile = 38;
			act = function(s)
				p [[¡Hay algo detrás del tapiz!]];
			end;
			used = function(s, w)
				if w == knife then
					s:disable();
					p [[¡He arrancado el tapiz!]];
					exist 'teleport':enable()
					exist 'block':disable()
					exist 'wdoor':enable()
				end
			end;
		},

		box(1, 7),
		box(1, 6),
		box(2, 7, 'shovel');

		tobj {
			nam = 'fireplace';
			x = 4;
			y = 1;
			obstacle = true;
			tile = 36;
			var { seen = false };
			act = function(s)
				if not taken(notes) then
					s.seen = true
					p [[En lo profundo de la chimenea, detrás del fuego, distingo un objeto.]];
					return
				end
				p [[Llama siniestra...]]
			end;
			used = function(s, w)
				if not s.seen then
					return
				end
				if w == rat then
					return "-- ¿Tienes hambre? ¡Puedes matarme de inmediato. ¡No me meteré en el fuego!"
				end

				if w == topor then
					if taken 'notes' then
						return [[¿Quemar el hacha?]]
					end
					return "El mango del hacha es demasiado corto."
				end
				if w == fakel then
					if taken 'notes' then
						return [[La antorcha ya está encendida.]]
					end
					return "La antorcha no llega a la parte trasera de la chimenea."
				end
				if w == shovel then
					if not taken(notes) then
						take(notes)
						return "Saqué el artículo del fuego con una pala. Resultó ser un fajo de papel medio quemado enrollado en un tubo apretado.";
					else
						return "Rastrillé las cenizas un poco con una pala."
					end
				end
			end;
		},
	};
}
