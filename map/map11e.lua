function tree(x, y)
	return tobj {
		nam = 'tree';
		x = x;
		y = y;
		tile = 8;
		obstacle = true;
		act = [[Viejo árbol seco...]];
	}:breakable("El árbol estaba completamente podrido.");
end

function stone(x, y)
	return tobj {
		nam = 'stone';
		x = x;
		y = y;
		tile = 51;
		obstacle = true;
		act = [[Es muy pesado...]];
	};
end

moving = function(s)
--	if me().x == 0 or me().y == 0 or me().x == 8 or me().y == 8 then
--		return
--	end
	if player_moved() then
		return
	end
	if isWall(ACT_X, ACT_Y) then
		return
	end
	if ACT_X == 0 and me().x ~= ACT_X then
		if here().w then
			me().x = 8
			return walk(here().w)
		end
	elseif ACT_X == 8 and me().x ~= ACT_X then
		if here().e then
			me().x = 0
			return walk(here().e)
		end
	elseif ACT_Y == 0 and me().y ~= ACT_Y then
		if here().n then
			me().y = 8
			return walk(here().n)
		end
	elseif ACT_Y == 8 and me().y ~= ACT_Y then
		if here().s then
			me().y = 0
			return walk(here().s)
		end
	end
end

edgen = tobj {
	nam = 'n';
	x = 0;
	y = 0;
	w = 9;
	h = 1;
	hidden = true;
	act = moving;
};
edges = tobj {
	nam = 's';
	x = 0;
	y = 8;
	w = 9;
	h = 1;
	hidden = true;
	act = moving;
};

edgee = 	tobj {
	nam = 'e';
	x = 8;
	y = 0;
	w = 1;
	h = 9;
	hidden = true;
	act = moving;
};
edgew = tobj {
	nam = 'w';
	x = 0;
	y = 0;
	w = 1;
	h = 9;
	hidden = true;
	act = moving;
};

map11e = troom {
	nam = 'Isla';
	refer = 'map11w';
	s = 'map11se';
	n = 'map11ne';
	w = 'map11w';
	bg = {
		7,7,7,7,7,7,7,0,0,
		7,7,7,7,7,7,7,7,0,
		7,7,7,7,7,7,7,7,0,
		7,7,0,0,0,7,7,7,0,
		7,7,0,7,0,0,0,0,0,
		7,7,0,7,7,7,7,0,0,
		7,7,7,7,7,7,7,7,0,
		7,7,7,7,7,7,7,7,0,
		7,7,7,7,7,7,7,7,0,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		12,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		0,0,0,0,0,0,0,11,11,
		0,0,0,0,0,0,0,0,11,
		0,0,0,0,0,0,0,0,11,
		0,0,11,11,11,0,0,0,11,
		0,0,11,0,11,11,11,11,11,
		0,0,11,0,0,0,0,11,11,
		0,0,0,0,0,0,0,0,11,
		0,0,0,0,0,0,0,0,11,
		0,0,0,0,0,0,0,0,11,
	};
	obj = {
		stone(1, 7);
		tree(2, 2);
		tree(4, 5);
		tree(7, 3);
		'edges', 'edgen', 'edgee', 'edgew',
	};
}
