heart = tobj {
	nam = 'corazón';
	tile = 53;
	inv = '¡El corazón del maldito Dactro!';
	obstacle = true;
	var { time = 16 };
	nouse = "¿Por qué necesita su corazón de brujo aquí?";
	life = function(s)
		s.time = s.time - 1
		if s.time == 0 then
			lifeoff(s)
			if have 'arabella' and here() ~= map10 and here() ~= map12 then
				walkin 'happyend'
				return
			end
			gameover.tile = 53;
			if here() == map10 or here() == map12 then
				p [[De repente, se oyó un ruido terrible y las paredes de la torre se derrumbaron,
				cayendo en pedazos. Terminé enterrado bajo las piedras. ¡Lo siento, Arabella, no pude salvarte!]]
			else
				p [[El suelo bajo mis pies tembló con una fuerza cada vez mayor. ¡Miré la torre y la vi derrumbarse!
				Los fragmentos de las paredes se desmoronaron ante mis ojos. ¿Qué he hecho? ¡Ahí estaba mi Arabella!
				¡Lo siento, no llegué tiempo!]]
			end

			walkin 'gameover'
		else
			if here() == map10 or here() == map12 then
				p [[Puedo sentir el suelo tiembla bajo mis pies...]]
			else
				p [[Puedo sentir la tierra temblando bajo mis pies...]]
			end
		end
	end;
	used = function(s, w)
		if w == knife then
			p "Tan pronto como perforé este corazón con un cuchillo,"
		elseif nameof(w) == 'fireplace' or nameof(w) == 'fire' then
			p "Tan pronto como arrojé este corazón al fuego,"
		elseif w == topor then
			p "Tan pronto como corté este corazón con un hacha,"
		elseif w == fakel then
			p "¿Freír esto un poco?"
			return
		else
			return
		end
		map10.noactive = true
		remove(s, me())
		put(blood, map12)
		blood.x = daktro.x
		blood.y = daktro.y
		daktro:disable()
		replace(rat, cat, me())
		if here() == map10 or here() == map12 then
			p "el suelo de la torre tembló."
		else
			p "los relámpagos brillaron sobre la torre y el trueno se extendió sobre la isla."
		end
		if here() == map12 then
			p [[La figura negra del hechicero se encogió y cayó el suelo.]];
		end
		lifeon(s);
	end;
	use = function(s, w)
		if nameof(w) == 'fireplace' or nameof(w) == 'fire' then
			return stead.call(s, 'used', w)
		end
		if w == rat then
			return [[-- ¡Aleja esa cosa asquerosa de mí! ¡Me da miedo la sangre!]]
		end
		if w == skeleton then
			if live(w) then
				p [[Al ver el corazón, el esqueleto se detuvo.]]
				lifeoff(w)
			end
		end
	end;
	act = function(s)
		p [[No sin estremecerme, tomé un corazón palpitante en mis manos.]]
		take(s)
	end
}

hbox = box(5, 3, 'heart');

map11nw = troom {
	nam = 'Isla';
	s = 'map11w';
	e = 'map11ne';
	refer = 'map11w';

	bg = {
		0,0,0,0,0,0,0,0,0,
		0,0,7,7,7,0,0,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
	};
	wall = {
		11,11,11,11,11,11,11,11,11,
		11,11,0,0,0,11,11,0,0,
		11,0,0,0,0,0,11,0,0,
		11,0,0,0,0,0,11,0,0,
		11,0,0,0,0,0,11,0,0,
		11,0,0,0,0,0,11,0,0,
		11,0,0,0,0,0,11,11,0,
		11,0,0,0,0,0,0,11,0,
		11,0,0,0,0,0,0,0,0,
	};
	obj = {
		tree(4, 2);
		stone(5, 4);
		tobj {
			nam = 'well';
			x = 2;
			y = 7;
			obstacle = true;
			act = [[Tan seco como los árboles...]];
			tile = 9;
		},
		tobj {
			nam = 'hole';
			x = 5;
			y = 3;
			var { tile = false };
			used = function(s, w)
				if w == shovel and heart_know then
					if not s.tile then
						p [[Empecé a cavar.... Unos minutos más tarde, la pala golpeó una tapa de una caja con un ruido.]]
						putf(hbox)
						s.tile = 4
					else
						p [[Ya he cavado lo suficiente.]]
					end
				end
				return true
			end
		};
		tree(2, 4);
		tree(7, 2);
		tree(6, 7);
		'edges', 'edgen', 'edgee', 'edgew',
	};
}
