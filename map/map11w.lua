map11w = troom {
	nam = 'Isla';
	refer = 'map11w';
	entered = function(s)
		if not have 'arabella' then
			set_music 'mus/1.ogg'
		end
	end;
	e = 'map11e';
	n = 'map11nw';
	s = 'map11sw';
	var { visited = false; };
	dsc = function(s)
		if not s.visited then
			s.visited = true
			p [[Salí corriendo del portal. Había hierba seca debajo de mis pies. Arriba: el cielo estaba cubierto de nubes negras.
			Por sorpresa, me mareé.^
			Miré a mi alrededor. La rata y yo estábamos en una pequeña isla.^
			La torre negra era visible y desde aquí, una siniestra luz roja ardía como un faro.^
			¿Qué estamos haciendo en este lugar con Arabella allí en la torre negra?]]
		else
			if stead.here() == map11w then
				p [[Estamos en una pequeña isla. Aquí se encuentra el portal que conecta con la torre.]]
			elseif stead.here() == map11nw then
				p [[Estamos ubicados en una pequeña isla. Aquí hay un arroyo que fluye hacia el mar.]];
			elseif stead.here() == map11sw then
				p [[Estamos en una pequeña isla. En la orilla se apilan grandes piedras.]]
			elseif stead.here() == map11e then
				p [[Estamos en una pequeña isla. La torre negra es claramente visible desde aquí. Un pequeño arroyo que fluye hacia el mar.]];
			elseif stead.here() == map11ne then
				p [[Estamos en una pequeña isla. El paisaje sombrío es deprimente.]];
			elseif stead.here() == map11se then
				p [[Estamos en una pequeña isla. Aquí se encuentra otro portal.]];
			end
		end
	end;
	active = { [[-- Aquí no hay ganas de hablar, ¿verdad?^-- Sí, la isla guarda su secreto.]]};
	lifes = { [[Las olas golpean la costa con ruido.]], [[Oigo gritos de gaviotas.]], [[Escucho el estruendo del trueno.]], [[La Luna amarilla apareció debido a las nubes negras.]],
		[[Creo que puedo ver la luz roja temblando en la parte superior de la torre.]] },
	bg = {
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
		0,7,7,7,7,7,7,7,7,
	};
	ground = {
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,12,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		11,0,0,0,0,0,0,0,0,
		11,11,0,0,0,0,0,0,0,
		11,11,0,0,0,34,33,0,0,
		11,11,0,0,0,0,31,0,0,
		11,11,0,0,0,0,0,0,0,
		11,11,0,0,0,0,0,0,0,
		11,11,0,0,0,0,0,0,0,
		11,0,0,0,0,0,0,0,0,
		11,0,0,0,0,0,0,0,0,
	};
	obj = {
		stone(4, 7);
		tobj {
			nam = 'pass';
			x = 7;
			y = 2;
			tile = 30;
		},
		tobj {
			nam = 'boat';
			x = 1;
			y = 6;
			used = function(s, w)
				if w == topor or w == fakel then
					p [[Este barco puede ser útil.]]
				end
			end;
			act = function(s)
				if visited 'happyend' then
					if have 'arabella' then
						return [[¡Arabella está inconsciente! ]]
					end
					walkin 'happyend2'
				else
					p [[Este barco podría ser útil para mí y para Arabella.]];
				end
			end;
			tile = 15;
		},
		tree(6, 5);
		tree(7, 7);
		tree(2, 2);
		'edges', 'edgen', 'edgee', 'edgew',
	};
}
