cat = tobj {
	nam = 'cat';
--	obstacle = true;
	tile = 57;
	var {x = 0, y = 0};
	inv = function(s)
		walkin(catdlg)
	end;
	used = function(s, w)
		if w == knife or w == topor or w == fakel then
			return [[¿Por qué atormentar a la desafortunada criatura?]]
		end
	end;
	life = function(s)
		local x, y
		if not here().troom_type then
			return
		end
		if player_moved() then
			if not seen(s) then
				put(s)
			end
			s.x, s.y = near(me(), here())
		end
		x, y = seek(s.x, s.y, me().x, me().y)
		if x == arabella.x and y == arabella.y then
			return
		end
		if x == me().x and y == me().y then
			return
		end
		s.x, s.y = x, y
	end;
}


catdlg = dlg {
	nam = 'Gato';
	hideinv = true;
	nolife = true;
	left = function(s, t)
		drop(cat, t)
		s.x, s.y = near(me(), t)
		lifeon(cat)
	end;
	entered = function(s)
	end;
	tile = 57;
	phr = {
		{ tag = 'cat1', [[¡Así que eso es lo que eras!]],
		[[No lo era, siempre he sido un gato, ¡incluso con esta vergonzosa apariencia!]],
		[[pon 'cat2']] },
		{ tag = 'cat2', false,  [[¿Todavía puedes hablar?]],
		[[-- Una vez que has aprendido a hablar, es imposible olvidarlo.]] },
		{ tag = 'cat', [[¿Y ahora qué?]],
		    function(s)
				if visited(happyend) then
					p [[-- A casa, tenemos que encontrar el camino a casa.]];
				else
					p [[-- ¡Debemos apresurarnos! Al destruir el corazón del hechicero, has destruido al propio hechicero,
					y al vencerle, has destruido su magia. No tenemos mucho tiempo,
					¡la Torre Negra está a punto de derrumbarse!]];
				end
		    end;
		},
		{},
	}
}

daktro = tobj {
	nam = 'daktro';
	var { x = 3;
	y = 5;};
	float = true;
	obstacle = true;
	tile = 54;
	life = function(s)
		if not here().troom_type then
			return
		end
		s.x, s.y = seek(s.x, s.y, me().x, me().y)
		if s.x == me().x and s.y == me().y then
			p [[Dactro estaba muy cerca. Sus manos huesudas y pálidas se acercaron a mi cara...
			--Mi corazón... -- escuché el susurro de su voz.]]
			gameover.tile = 54;
			walkin 'gameover'
		end
	end;
}
near = function(s, w)
	local x = s.x
	local y = s.y
	if x ~= 8 and not isSomething(x + 1, y, w) then
		x = x + 1
	elseif x ~= 0 and not isSomething(x - 1, y, w) then
		x = x - 1
	elseif y ~= 8 and not isSomething(x, y + 1, w) then
		y = y + 1
	elseif y ~= 0 and not isSomething(x, y - 1, w) then
		y = y - 1
	end
	return x, y
end
arabelladlg = dlg {
	nam = 'Arabella';
	hideinv = true;
	nolife = true;
	entered = function(s)
	end;
	tile = 55;
	left = function(s, t)
		pon('b')
		if have(arabella) then
			drop (arabella, t)
			lifeon(arabella)
			arabella.x, arabella.y = near(me(), t)
		end
	end;
	phr = {
		{ tag = 'a', [[¡Arabella, Arabella! ¿Estás viva? ¡Ya pasó todo!]],
		[[-- Oh, ¿dónde estoy? -- Arabella abrió los ojos. ¡Está viva!]], [[pon 'b']] },
		{ tag = 'b', false, [[¡Todo va a estar bien!]],
			[[-- ¡Sabía que vendrías a por mí, mi héroe!]]};
		{},
	}
}

arabella = tobj {
	nam = 'arabella';
--	obstacle = true;
	var {
		x = 4;
		y = 3;
	};
	tile = 55;
	inv = function(s)
		if visited(happyend) then
			walkin 'arabelladlg'
		else
			p [[Arabella está inconsciente.]];
		end
	end;
	life = function(s)
		local x, y
		if not here().troom_type then
			return
		end
		if player_moved() then
			if not seen(s) then
				put(s)
			end
			s.x, s.y = near(me(), here())
		end
		x, y = seek(s.x, s.y, me().x, me().y)
		if x == me().x and y == me().y then
			return
		end
		if x == cat.x and y == cat.y then
			return
		end
		s.x, s.y = x, y
	end;
	used = function(s, w)
		if w == knife or w == topor or w == fakel then
			p [[Este juego no es para sádicos.]]
		end
		if w == cat or w == rat then
			p [[-- ¡No estoy domesticado!]]
		end
	end;
	act = function(s)
		if visited(happyend) then
			return [[¡Mi Arabella!]]
		end
		local t = exist 'tree'
		if not t.knife then
			return "¡Arabella está atada a un pilar!"
		end
		take(s)
		return "Puse a Arabella en mi hombro."
	end
}

fire = function(x, y)
	return tobj {
		nam = 'fire';
		x = x;
		y = y;
		tile = 47;
		obstacle = true;
		act = [[¡La llama poderosa!!]];
	}
end

map12 = troom {
	nam = 'noveno piso';
	nodsc = true;
	dsc = function(s)
		p [[¡Y aquí estamos, en la cima de la Torre Negra!]]
		if disabled(daktro) then
			p [[El cuerpo del hechicero negro está tirado en el suelo.
			  No necesitaba un corazón, pero cuando lo perdió, perdió la vida.]]
		else
			p [[¡El hechicero negro Dactro se nos acerca!]]
		end
		p [[El fuego diabólico arde por todas partes.]]
		if not taken(arabella) then
			p [[¡Arabella está atada a un poste! Maldito brujo, espero que esté viva!]]
		end
	end;
	entered = function(s)
		me().x = 7
		me().y = 7
		lifeon(daktro)
	end;
	left = function(s)
		lifeoff(daktro)
	end;
	bg = {
		0,0,0,0,0,0,0,0,0,
		0,18,18,18,18,18,18,18,0,
		0,18,46,46,46,46,46,18,0,
		0,18,46,46,46,46,46,18,0,
		0,18,46,46,18,46,46,18,0,
		0,18,46,46,46,46,46,18,0,
		0,18,46,46,46,46,46,18,0,
		0,18,18,18,18,18,18,18,0,
		0,0,0,0,0,0,0,0,0,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,48,0,0,0,48,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,48,0,0,0,48,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		1,1,1,1,1,1,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,1,1,1,1,1,1,1,1,
	};
	obj = {
		daktro,
		tobj {
			nam = 'out';
			x = 7;
			y = 7;
			tile = 17;
			act = function(s)
				if not have(arabella) then
					return
				end
				if have 'heart' and not disabled(daktro) then
					p [[Corrí por los escalones, pero al escuchar el ruido detrás de mí,
					me di cuenta de que había cometido un error... El maldito hechicero]];
					if have 'arabella' then
						p [[nos arruinó.]]
					else
						p [[me arruinó.]]
					end
					gameover.tile = 47;
					walkin 'gameover'
					return
				end
				walk 'map10'
			end
		},
		win(8, 7);
		win(8, 3);
		win(8, 1);
		win(7, 0);
		win(5, 0);
		win(3, 0);
		win(1, 0);
		win(0, 1);
		win(0, 3);
		win(0, 5);
		win(0, 7);
		win(1, 8);
		win(3, 8);
		win(5, 8);
		win(7, 8);
		fire(6, 6);
		fire(2, 6);
		fire(2, 2);
		fire(6, 2);

		tobj {
			nam = 'tree';
			obstacle = true;
			x = 4;
			y = 3;
			var { knife = false };
			tile = 56;
			used = function(s, w)
				if w == topor then
					if not s.knife then
						p [[¡Puedo hacerle daño a Arabella!]]
					else
						s:disable()
						p [[¡Maldito pilar!]]
						return
					end
					return
				end
				if w == knife then
					if not s.knife then
						s.knife = true
						return "Corté las cuerdas."
					else
						return "Ya corté las cuerdas."
					end
				end
			end
		},
		arabella,
		tobj {
			nam = 'drova';
			x = 4;
			y = 4;
			tile = 48;
			used = function(s, w)
				if w == fakel then
					p [[Es una mala idea.]]
				end
			end
		},
	};
}

happyend = room {
	nam = 'Final';
	hideinv = true;
--	scaledpic = 'gfx/end.png';
	pic = 'gfx/end4x.png';
	nolife = true;
	entered = function(s)
		set_music 'mus/6.ogg'
	end;
	dsc = [[La tierra bajo mis pies sacudió con fuerza cada vez mayor. ¡Miré la torre y la vi desmoronarse! Los restos de las paredes se desmoronaron ante mis ojos.
		¡Lo logramos a tiempo! Arabella aún no mostraba señales de vida, pero yo sabía... sabía que todo había terminado.^
		Definitivamente regresaremos a casa y todo estará bien.^
		Las nubes negras sobre mi cabeza comenzaron a disiparse gradualmente y vi las estrellas por primera vez en esta noche.^
		Eran muchas. Parpadeaban sin prisa y amistosamente en el frío de la noche, alegrándose con nosotros.^^
		{b|Siguiente}]];
	obj = { xact("b", code [[ walkout() ]]) };
}

--happyend2 = room {
--	nam = 'Конец';
--	hideinv = true;
--	nolife = true;
--	dsc = [[Happy end 2]];
--}
