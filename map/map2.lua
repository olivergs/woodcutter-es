game.timer = function()
	return true
end
ratdlg2 = dlg {
	nam = 'Rata';
	hideinv = true;
	nolife = true;
	left = function(s)
		pon('unknown', 'l3', 'l4', 'l5', 'l5_1', 'l6', 'l7', 'l8', 'l9', 'l10', 'l11')
	end;
	entered = function(s)
		if from() == map4 then
			pstart 'l3'
		elseif from() == map3 then
			pstart 'l4'
		elseif from() == map5 then
			pstart 'l5'
			if taken 'key' then
				pstart 'l5_1'
			end
		elseif from() == map6 then
			if visited 'stopplz' and not isObstacle(6, 7, from()) then
				pon 'l6_1'
			else
				poff 'l6_1'
			end
			pstart 'l6'
		elseif from() == map7 then
			pstart 'l7'
		elseif from() == map8 then
			pstart 'l8'
		elseif from() == map9 then
			pstart 'l9'
		elseif from() == map10 then
			pstart 'l10'
		elseif nameof(from()) == 'Остров' then
			pstart 'l11'
		else
			pstart 'unknown'
		end
	end;
	tile = 19;
	phr = {
		{tag = 'unknown', '¿Cómo estás?',
		'-- Estoy bien.'};
		{},
		{tag = 'l3', '¿Puede decirme algo sobre este esqueleto?',
		'-- El hechicero los hechizó para matar a los vivos. Son muy peligrosos, aunque estúpidos.'};
		{},
		{tag = 'l4', 'El piso es extraño...',
		'-- Sí, el brujo tiene un peculiar sentido del humor.'};
		{},
		{tag = 'l5', '¿Tienes alguna idea de cómo ir al siguiente piso?...',
		'-- Necesitamos la llave de la puerta de la escalera...'};
		{},
		{tag = 'l5_1', '¡Encontré la llave!',
		'-- Espero que nos ayude.'};
		{},
		{ tag = 'l6', [[¿Tiene alguna idea sobre cómo salir de aquí?]],
			function(s)
				if isObstacle(6, 7, from()) then
					p [[-- ¡Salgamos de aquí!]]
				else
					p [[-- Creo que la puerta se abre con una losa.]]
				end
			end},
		{ tag = 'l6_1', false, [[Mira, ¿por qué no te quedas junto a la losa mientras yo me encargo de Daktro?]],
			'-- ¡¡¡Nunca!!!'};
		{},
		{ tag = 'l7', [[Estoy completamente confundido, ¿qué son estas losas en el suelo?]],
			[[-- ¿Sabes?, mi punto fuerte es la intuición, y me dice que la losa mas desgastada es un comienzo.^
			-- ¿El comienzo de qué?^
			-- Um... El comienzo del viaje.^
			-- ???]] };
		{},
		{ tag = 'l8', [[¿Tienes miedo de las arañas?]],
			function(s)
				if disabled(spider) then
					p [[-- Pfff. ¡Por supuesto no!]];
				else
					p[[-- ¿Por qué lo preguntas?]]
				end
			end};
		{},
		{ tag = 'l9', [[¿Qué te parece este pilar en el centro? ]],
			function(s)
				if have 'key' then
					p [[-- Adiviné desde el principio cómo funciona.]]
				else
					p [[-- ¡Ya me has torturado, el volumen de mi cerebro es cien veces menor que el tuyo!]];
				end
			end
		};
		{},
		{ tag = 'l10', [[Mira, el camino hacia arriba está despejado.]],
			function(s)
				if not have 'heart' then
					p [[-- Mi intuición me dice que hay peligro en ese lugar...]]
				else
					p [[-- Sí, ahora estamos listos para enfrentarnos a Dactro. Mientras tengamos su corazón, no se atreverá a quemarnos. Sin embargo, puede matarnos de todos modos.]];
				end
			end; },
		{},
		{ tag = 'l11', [[¿Qué estamos haciendo aquí?]],
			function(s)
				if not heart_know then
					p [[-- No tengo ni idea...]]
				else
					if not have 'heart' then
						p [[-- Creo que esta isla guarda un terrible secreto...]]
					else
						p [[-- Creo que es hora de volver a la torre.]];
					end
				end
			end
		};
		{},
	}
}
waitdlg = dlg {
	nam = 'Rata';
	nolife = true;
	hideinv = true;
	entered = function(s)
		seen('trigger', map2):disable();
		p '-- ¡Espera! - la rata me llamó.';
	end;
	exit = function(s)
		lifeon(rat)
		timer:set(300)
		rat._movie = true
	end;
	tile = 19;
	phr = {
		{'???',
			code [[ psub 'ask']] };
		{'¡Tengo que apurarme!',
			code [[ psub 'ask']] };
		{},
		{ tag = 'ask', '-- ¿Por qué estás aquí? ¿Qué te hizo el hechicero Dactro?'},
		{'Dactro ha secuestrado a mi esposa.',
			code [[ psub 'ask2' ]]},
		{ 'No es asunto tuyo, tengo una cuenta personal que saldar con el brujo.',
			code [[ psub 'ask2' ]]},
		{},
		{ tag = 'ask2', '-- Sabes, he estado pensando. Llévame contigo, también quiero ver a este hechicero.' },
		{ always = true; tag = 'ok', 'Muy bien, entra en la bolsa.', '-- ¡No te arrepentirás, ya lo verás!', [[ pjump 'getout' ]] },
		{ 'No', code [[ pjump 'no2' ]] };
		{ },
		{ tag = 'ok2', always = true, 'No', '-- ¡Lo sabía! ¡No te arrepentirás, ya lo verás!', [[ pjump 'getout' ]] },
		{ tag = 'no2', always = true, '-- Por favor, llévame, ¿vale?' },
		{ always = true, 'No', code [[ pjump 'no3' ]] };
		{ alias = 'ok' },
		{ },

		{ tag = 'no3', '--¡Por favor, por favor!' },
		{ always = true, 'No', code [[ pjump 'no4' ]] };
		{ alias = 'ok' },
		{ },

		{ tag = 'no4', '-- ¿Por favor, por favor, por favor?' },
		{ always = true, 'No', code [[ pjump 'no5' ]] };
		{ alias = 'ok' },
		{ },

		{ tag = 'no5', '-- ¿Estás seguro?' },
		{ alias = 'ok2' };
		{ always = true, 'Sí', code [[ pjump 'no6']] },
		{ },

		{ tag = 'no6', '-- ¿Quieres que muera aquí solo?' },
		{ alias = 'ok2' };
		{ always = true, 'Sí', code [[ pjump 'no7']] },
		{ },

		{ tag = 'no7', '-- ¿Conoces todos los rincones de la torre como yo?' },
		{ alias = 'ok2' };
		{ always = true, 'Sí', code [[ pjump 'no8']] },
		{ },

		{ tag = 'no8', '-- Última oportunidad, ¿me llevarás?' },
		{ always = true, 'No', code [[ pjump 'no2' ]] };
		{ alias = 'ok' },
		{ },

		{ tag = 'getout'; 'Terminar la conversación', code [[ walkout()]] }
	}
}

local story = [[-- El hechicero de esta torre me hechizó y me dio una abominable apariencia... Es una historia triste,
y no hay tiempo para recordar... Estoy aquí en este maldito pozo, casi muerto
de hambre, pero no pudo encontrar una salida. Aunque tal vez con tu poder podamos
encontrar el camino de salida...]];

ratdlg = dlg {
	nam = 'Rata';
	hideinv = true;
	nolife = true;
	entered = "La rata se quedó paralizada y me miró expectante.";
	tile = 19;
	phr = {
		{'¡Aaaaa!',
			'-- ¿Por qué te asustas? -- responde la rata.', [[ psub 'ask']] };
		{'¡Hola!',
			'-- Bueno, hola, si no estás bromeando.', [[ psub 'ask']] };
		{always = 'true', 'Terminar la conversación', code [[ walkout()]] };
		{},
		{ tag = 'ask', '¿Sabes hablar?',
			'-- ¿Eso cambia algo?', [[ psub 'yes' ]] },
		{ '¿Cómo te llamas?',
			'-- ¿Importa?'},
		{ false, tag = 'alive', 'Te ves bastante alegre, para ser una rata medio muerta...',
			'-- Estoy nervioso.' },
		{},
		{ tag = 'yes', '¡Sí, me interesa mucho!',
			story, [[ exist ('out', map2): enable(); pjump 'way';  ]] },
		{ 'No, es mucho más interesante salir de aquí.',
			[[--En vano, hombre, llegaste aquí. -- la rata sacudió la cabeza.^]]..story, [[ exist ('out', map2):enable();pjump 'way';   ]] },

		{},
		{ tag = 'way', '¿Un camino?',
			[[-- Sé que el sótano de la torre está en algún lugar cercano y al mismo nivel que el fondo del pozo... Aunque, ya sabes,
			es mejor dejar toda esperanza y morir juntos... No tenemos ninguna posibilidad.]],
				[[ exist('blocka', map2):disable();
				    exist('blockb', map2):disable();
				    exist('block1', map2):enable();
				    exist('block2', map2):enable(); pon 'alive']]},
		{},
	}
}

blood = tobj {
	nam = 'blood';
	var {x = 0, y = 0; };
	tile = 20;
	used = true;
	act = true;
}

rat = tobj {
	var { x = 3; y = 5 };
	var { ddx = -1; };
	tile = 19;
	nouse = 'Es difícil imaginar cómo puede ayudar una rata aquí.';
	obstacle = true;
--	dsc = "По дну колодца бегает большая крыса.";
	life = function(s)
		if here() ~= map2 then
			return
		end
		if s._movie then
			if s.y > 3 then
				s.y = s.y - 1
				return true
			end
			if s.x < me().x - 1 then
				s.x = s.x + 1
				return true
			end
			if s.y ~= 2 then
				s.y = s.y - 1
				return true
			end
			if s.x ~= me().x then
				s.x = s.x + 1
				return true
			end
			take(s)
			lifeoff(s)
			timer:stop()
		end
		if s.x + s.ddx == me().x and s.y == me().y then
			return
		end
		s.x = s.x + s.ddx
		if s.x > 5 then
			s.x = 5
			s.ddx = -1
		elseif s.x < 3 then
			s.x = 3
			s.ddx = 1
		end
	end;
	used = function(s, w)
		if w == topor then
			if taken(s) or seen('out', map2) then
				return '¿Por qué matar a la pobre criatura?'
			end
			lifeoff(rat)
			rat:disable()
			put(blood)
			blood.x = rat.x
			blood.y = rat.y
			return "¡Muere, maldita cosa!"
		end
	end;
	inv = function(s)
		walkin(ratdlg2)
	end;
	act = function()
		if here() == map6 or here() == map9 then
			take(rat)
			return
		end
		walkin(ratdlg)
	end
}

map2 = troom {
	nam = 'Pozo';
	left = function(s) if not have 'topor' then take 'topor'; p 'Al salir del pozo, tomé mi hacha.' end  restore_music() end;
	entered = function(s)
		me().x = 4
		me().y = 4
		lifeon(rat)
		save_music();
		set_music 'mus/3.ogg'
		p "La longitud de la cuerda no era suficiente. No hay vuelta atrás."
	end;
	dsc = [[Me encontré en el fondo de un pozo seco. Se está relativamente tranquilo aquí,
	sin el zumbido del viento. Cuando mis ojos se acostumbraron a la oscuridad,
	vi a una rata que corría de un lado a otro por el fondo del pozo.]];
	dsc2 = [[Oscuro y sofocante. Las paredes ásperas del pozo están a mi alrededor.]];
	lifes = { [[Puedo escuchar el zumbido del viento bajando por el pozo...]] };
	bg = {
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
		4,4,4,4,4,4,4,4,4,
	};
	wall = {
		0,0,0,0,4,4,4,4,0,
		0,0,0,0,4,0,0,0,0,
		0,0,1,1,1,0,0,0,4,
		0,0,1,0,0,0,1,4,4,
		0,0,1,0,0,0,1,0,0,
		0,0,1,0,0,0,1,0,0,
		0,0,1,1,1,1,1,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	obj = {
		tobj {
			nam = 'blocka';
			x = 5;
			y = 2;
			obstacle = true;
			tile = 1;
			weight = 2;
			used = true;
		},
		tobj {
			nam = 'blockb';
			x = 6;
			y = 2;
			obstacle = true;
			tile = 1;
			weight = 2;
			used = true;
		},
		tobj {
			nam = 'block1';
			var { x = 5;
			y = 2; };
			obstacle = true;
			tile = 1;
			weight = 2;
			used = true;
		}:movable():disable(),
		tobj {
			nam = 'block2';
			var { x = 6;
			y = 2; };
			obstacle = true;
			tile = 1;
			weight = 2;
			used = true;
		}:movable():disable(),
		tobj {
			nam = 'out';
			x = 8;
			y = 0;
			obstacle = true;
			tile = 17;
			act = function()
				walk 'map4'
			end
		}:disable(),
		'rat';
		tobj {
			nam = 'trigger';
			x = 6;
			y = 1;
			act = function()
				walkin 'waitdlg'
			end
		},
	};
}
