function obstacle(name, tile, x, y)
	return tobj {
		nam = name;
		tile = tile;
		x = x;
		y = y;
		used = true;
		obstacle = true;
	}
end
local sact = "¡¡¡Esqueleto!!!";
skeleton1 = obstacle('skel1', 21, 1, 1)
skeleton2 = obstacle('skel2', 21, 2, 1)
skeleton3 = obstacle('skel3', 21, 3, 1)
skeleton4 = obstacle('skel4', 21, 5, 1)
skeleton5 = obstacle('skel5', 21, 6, 1)
skeleton6 = obstacle('skel6', 21, 7, 1)
skeleton1.act = sact;
skeleton2.act = sact;
skeleton3.act = sact;
skeleton4.act = sact;
skeleton5.act = sact;
skeleton6.act = sact;

spikes = tobj {
	nam = 'spikes';
	var {x = 0, y = 0; };
	tile = 25;
	used = true;
	act = true;
}

local doblock = function(s)
	me().x = s.x
	me().y = s.y
end

win = function(x, y)
	return tobj {
		nam = 'window';
		x = x;
		y = y;
		tile = 6;
		float = true;
	}
end

map3 = troom {
	nam = 'Segunda planta';
	dsc = [[No sin alivio subimos al segundo piso... Esta vez no había esqueletos !!!
	Sólo unas extrañas losas bloqueaban el paso al siguiente nivel.^
	Después de mirar más de cerca, por alguna razón sentí que también había que tener cuidado aquí...]];
	active = { [[Extrañamente, parece que las placas ya familiares en el suelo se tambalean un poco...]] };
	lifes = { [[El viento irrumpió a través de las estrechas ventanas y sopló el polvo de las extrañas losas en el suelo.]] };
	dsc2 = [[El extraño mecanismo de las losas me asusta.]];
	entered = function(s)
		me().x = 4
		me().y = 8
		lifeon(s)
	end;
	left = function(s)
		lifeoff(s);
	end;
	life = function(s)
		local b1 = seen 'block1'
		local b2 = seen 'block2'
		if me().y < 4 then
			return
		end
		if me()._oldx == me().x and me()._oldy == me().y then
			return
		end
		me()._oldx = me().x
		me()._oldy = me().y
		if not b1._dx then b1._dx = 1 end
		if not b2._dx then b2._dx = -1 end
		if me().x % 2 == 0 then
			b1.x = b1.x + b1._dx
			if b1.x > 7 then
				b1._dx = - b1._dx
				b1.x = 6
			elseif b1.x < 1 then
				b1._dx = - b1._dx
				b1.x = 2
			end
		end

		if me().y % 2 == 0 then
			b2.x = b2.x + b2._dx
			if b2.x < 1 then
				b2._dx = - b2._dx
				b2.x = 2
			elseif b2.x > 7 then
				b2._dx = - b2._dx
				b2.x = 6
			end
		end

	end;
	bg = {
		18,18,18,18,4,18,18,18,18,
		18,18,18,18,18,18,18,18,18,
		18,26,26,26,26,26,26,26,18,
		18,26,26,26,26,26,26,26,18,
		18,18,18,18,18,18,18,18,18,
		18,18,18,18,18,18,18,18,18,
		18,18,18,18,18,18,18,18,18,
		18,18,18,18,18,18,18,18,18,
		18,18,18,18,18,18,18,18,18,
	};
	ground = {
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,
	};
	wall = {
		1,1,1,1,0,1,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,1,1,2,0,3,1,1,1,
	};
	obj = {
		win (2, 0);
		win (6, 0);
		tobj {
			nam = 'out';
			x = 4;
			y = 0;
			tile = 17;
			obstacle = true;
			act = code [[ walk 'map5' ]];
		},
		tobj {
			nam = 'block1';
			stop = true;
			var { x = 1;
			y = 3; };
			tile = 27;
			act = doblock;
		},
		tobj {
			nam = 'block2';
			stop = true;
			var { x = 7;
			y = 2; };
			tile = 27;
			act = doblock;
		},
		tobj {
			nam = 'hole';
			x = 1;
			y = 2;
			w = 7;
			h = 2;
			act = function(s)
				p 'De repente, unos pinchos de hierro surgen debajo de sus pies';
				hero:disable();
				put(blood)
				put(spikes)
				blood.x = ACT_X
				blood.y = ACT_Y
				spikes.x = ACT_X
				spikes.y = ACT_Y
			end;
		};
		box(1, 1, 'skeleton1');
		box(2, 1, 'skeleton2');
		box(3, 1, 'skeleton3');
		box(5, 1, 'skeleton4');
		box(6, 1, 'skeleton5');
		box(7, 1, 'skeleton6');
	};
}
