key = tobj {
	var {x = 0, y = 0 };
	nam = 'key';
	tile = 28;
	tak = 'Tomé la llave';
	nouse = 'No encaja.';
}

wooden_door = function(x, y, n, wh)
	return tobj {
		nam = 'wdoor';
		var { broken = n };
		walk = wh;
		x = x;
		y = y;
		var { tile = 22; };
		act = function(s)
			if s.broken ~= 0 then
				return 'La robusta puerta de madera está cerrada.'
			end
			if s.walk then
				walk 'map3'
			end
		end;
		obstacle = true;
		used = function(s, w)
			if w == topor then
				if s.broken == 0 then
					return "Ahora no hay nada que romper en este lugar."
				end
				s.broken = s.broken - 1
				if s.broken == 0 then
					if s.walk then
						s.tile = 4;
					else
						s:disable()
					end
					put (new (string.format("garbage(%d, %d)", s.x, s.y)))
					return "Destruí la puerta con un hacha."
				else
					return "Golpeé la puerta con un hacha."
				end
			end
		end;
	}
end


function box(x, y, item)
	return tobj {
		nam = 'box';
		var { x = x;
		y = y;};
		tile = 23;
		obstacle = true;
		float = true;
	}:movable():breakable("Rompí el cajón.", item);
end

function garbage(x, y)
	return tobj {
		nam = 'garbage';
		x = x;
		y = y;
		tile = 48;
		obstacle = false;
	}
end

function block(x, y, tile)
	local b = box(x, y)
	b.tile = tile
	b.used = nil
	return b
end

gameover = room {
	nam = 'Конец';
	hideinv = true;
	nolife = true;
	var { tile = 1 };
	entered = function(s)
		set_music('mus/end.ogg', 1)
	end;
	dsc = [[{reload|Volver a jugar}]];
}

seen_trigger = function(s)
	if s.x == me().x then
		local a, b, yy
		a = s.y
		b = me().y
		if a > b then a,b = b, a end
		for yy = a, b do
			if yy == b then
				s.started = true
				break
			end
			if isObstacle(s.x, yy) and yy ~= s.y then
				break
			end
		end
	end

	if s.y == me().y and not s.started then
		local a, b, xx
		a = s.x
		b = me().x
		if a > b then a,b = b, a end
		for xx = a, b do
			if xx == b then
				s.started = true
				break
			end
			if isObstacle(xx, s.y) and xx ~= s.x then
				break
			end
		end
	end
end

function seek(x, y, tx, ty, wh)
	local dx = tx - x
	local dy = ty - y
	local ddx = 0
	local ddy = 0
	if not wh then wh = here() end
	if dx ~= 0 then
		if dx > 0 then ddx = 1 else ddx = -1 end
	end
	if dy ~= 0 then
		if dy > 0 then ddy = 1 else ddy = -1 end
	end

	if math.abs(dx) > math.abs(dy) and not isObstacle(x + ddx, y) then
		x = x + ddx
	elseif not isObstacle(x, y + ddy) then
		y = y + ddy
	elseif not isObstacle(x + ddx, y) then
		x = x + ddx
	end
	return x, y
end

skeleton = tobj {
	nam = 'skeleton';
	stop = true;
	float = true;
	var {
		x = 4;
		y = 1;
		started = false;
		fooled = false;
	};
	life = function(s)
		if not here().troom_type then
			return
		end
--		if taken 'heart' and not have 'heart' then
--			lifeoff(s)
--		end
		if s.started then
			s.x, s.y = seek(s.x, s.y, me().x, me().y)
			if s.x == me().x and s.y == me().y then
				p "El agarre del esqueleto estaba muerto."
				gameover.tile = 21;
				walkin 'gameover'
			end
			return
		end
		local m = seen 'mirror'
		if m and m.y == s.y and not s.fooled then
			s.fooled = true
			return
		end
		if s.fooled then
			s.x = s.x + 1
			if s.x == m.x - 1 then
				lifeoff(s)
			end
			return
		end
		seen_trigger(s)
	end;
	tile = 21;
	obstacle = true;
--	dsc = function(s)
--		if not s.fooled then
--			if here() == map4 then
--				p 'Путь наверх загораживает скелет.';
--			end
--		end
--	end
}

map4 = troom {
	nam = 'primer piso';
	dsc = [[¡Terminamos en el primer piso de la torre! El suelo estaba pavimentado con baldosas grises, cuyos dibujos geométricos resultaban enloquecedores.
    A nuestra izquierda había una puerta de madera, que probablemente conducía al siguiente piso. Pero esto no fue lo que llamó nuestra atención.^
		¡Había un esqueleto delante de la puerta de madera! ¡Sí, era un esqueleto!^
		No había nada más que un viejo espejo junto a la pared opuesta y una caja de madera.]];
	dsc2 = [[El patrón geométrico de las baldosas en el piso es perturbador. Un viejo espejo está contra la pared.]];
	entered = function(s)
		me().x = 1;
		me().y = 4;
		lifeon(skeleton)
		set_music 'mus/7.ogg'
	end;
	exit = function(s)
		lifeoff(skeleton)
	end;
	bg = {
		0,0,0,0,0,0,0,0,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,0,0,0,0,0,0,0,0,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		1,1,1,1,0,1,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		4,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,1,1,1,0,1,1,1,1,
	};
	obj = {
		tobj {
			nam = 'door1';
			x = 4;
			y = 8;
			tile = 5;
			obstacle = true;
			act = [[La puerta que conduce hacia afuera. Cerrada con llave.]];
		},
		wooden_door(4, 0, 2, 'map3');
		skeleton,
		box(2, 6);
		tobj {
			nam = 'mirror';
			var { x = 7;
			y = 6; };
			tile = 24;
			obstacle = true;
--			dsc = 'У стены стоит зеркало.';
		}:movable(),
	};
}
