
openable_door = function(x, y)
	return tobj {
			nam = 'door';
			var { opened = false };
			x = x;
			y = y;
			var { tile = 5; };
			var { obstacle = true; };
			act = function(s)
				if s.opened and s.obstacle then
					s.tile = 29;
					s.obstacle = false
					return 'Abrí la puerta.';
				end
				if not s.opened then
					p 'La puerta está cerrada con llave. Necesito una llave.';
				end
			end;
			used = function(s, w)
				if w == key then
					p [[Apareció la llave!]]
					remove(w, me())
					s.opened = true
				end
			end;
		}
end

map5 = troom {
	nam = 'tercer piso';
	dsc = [[-- ¡Ya está, no hay más losas! ¿Entiendes, Daktro? ¡Engañamos a tus losas! Um... Otra vez el esqueleto.
		No importa, Daktro, ya sabemos cómo manejar a tus fieles pero descerebrados guardias...^
		Yo mismo no me di cuenta de cómo empecé a hablar en voz alta.]];
	dsc2 = [[La geometría de la sala es completamente simétrica. El camino hacia arriba se encuentra en el centro de esta planta.]];
	lifes = {[[¿Cómo llegar al siguiente piso?!!]]};
	active = { [[El hechicero es un paranóico, de lo contrario, ¿por qué necesita todos estos enigmas y trampas?]],
		[[-- Escucha Rata, ¿o sería mejor llamarte "Rato"?^
		-- Déjame en paz, ¿eh?^
		-- ¿No tienes nombre?^
		-- Mi nombre murió con mi pasado ...^
		-- ¡No importa, nos vengaremos del malvado hechicero!]]};
	entered = function(s)
		me().x = 4
		me().y = 8
		skeleton.fooled = false
		skeleton.started = false
		skeleton.x = 4
		skeleton.y = 0
		lifeon(skeleton)
		set_music 'mus/4.ogg'
	end;
	left = function(s)
		lifeoff(skeleton)
	end;
	bg = {
		0,0,0,0,18,0,0,0,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,4,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,0,0,0,18,0,0,0,0,
	};
	wall = {
		1,1,1,2,0,3,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,32,0,32,0,0,3,
		2,0,0,31,0,31,0,0,3,
		2,0,0,3,1,2,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,1,1,2,0,3,1,1,1,
	};
	obj = {
		tobj {
			nam = 'out';
			x = 4;
			y = 4;
			tile = 17;
			act = code [[ walk 'map6' ]];
		},
		openable_door(4, 3);
		win(0, 2),
		win(0, 6),
		win(8, 2),
		win(8, 6),
		box(5, 1, 'key'),
		box(6, 1),
		box(7, 1),
		box(6, 2),
		box(7, 2),
		box(7, 3),
		skeleton,
	};
}
