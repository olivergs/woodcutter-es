stopplz = dlg {
	nam = 'Rata';
	nolife = true;
	hideinv = true;
	entered = function(s)
		p '-- ¡Espera! ¡No me dejarás aquí!!! -- la rata me llamó.';
	end;
	left = function(s)
		take(rat, map6);
	end;
	tile = 19;
	phr = {
		{always = true, 'Por supuesto que no.',  '-- ¡¡¡Lo sabía!!!',  ' walkout()' };
		{always = true, '¡Tendrás que quedarte aquí!', '-- ¡No, no me voy a quedar solo!', 'walkout()' };
		{},
	}
}

teleport = function(s)
	if me().x > s.x and me().y == 4 then
		me().x = 8
	elseif me().x < s.x and me().y == 4 then
		me().x = 0
	elseif me().y > s.y and me().x == 4 then
		me().y = 8
	elseif me().y < s.y and me().x == 4 then
		me().y = 0
	end
end

map6 = troom {
	nam = 'cuarto piso';
	dsc = [[En cuanto entré en esta habitación me disgustó su geometría. Casi simétrico,
	  con muchas esquinas y juna especie de pasillos en todos los extremos. Pero no hay vuelta atrás,
	  ¡¡¡tenemos que subir!!! En un rincón observé una extraña losa en el suelo.]];
	dsc2 = [[Esta habitación me aburre cada vez más. No puedo esperar a salir de aquí.]];
	lifes = { [[-- Mira, en esa esquina de allí...^
		-- ¿Dónde?^
		-- Parecio...]],
		[[-- Rata, pero aún así, ¿quién eras?^
		-- Ahora no importa...]],
		[[-- Mira, estos teletransportadores, ¿tienen algún significado?^
		-- No estoy seguro, Rata, ¿es necesario buscar lógica en la locura?^
		-- ¿De qué otra manera vamos a salir de aquí?]]};
	active = {
		[[-- Bueno, salgamos de aquí ya^
		 -- Estoy pensando ahora, Rata...^
		 -- ¿Qué hay que pensar? ¡Ssólo hay que abrir esa puerta!^]],
		[[-- No me gusta este lugar.^
		-- A mi tampoco.^
		-- ¿Se te ocurrirá algo pronto?^
		-- No te entrometas si no sabes cómo ayudar.]],
		[[-- Bueno, harás algo pronto?^
		-- No seas entrometida, Rata, creo...^
		-- Algo no cuadra.^
		-- ¿Tienes alguna idea?^
		-- ...]]};
	entered = function(s)
		me().x = 4
		me().y = 4
		lifeon(s)
	end;
	left = function(s)
		lifeoff(s);
	end;
	life = function(s)
		local d = seen 'door'
		if here() ~= s then
			return
		end
		if isObstacle(6, 7) then
			d.obstacle = false
			d.tile = 29
			return
		end
		if me().x == 6 and me().y == 7 then
			d.tile = 29
		else
			d.tile = 5
			d.obstacle = true
		end
	end;
	bg = {
		4,4,4,4,4,4,4,4,4,
		4,0,0,0,18,0,4,0,4,
		4,18,18,18,18,18,18,0,4,
		4,18,18,18,18,18,18,0,4,
		4,18,18,18,18,18,18,18,4,
		4,0,18,18,18,18,18,0,4,
		4,0,18,18,18,18,18,0,4,
		4,0,0,0,18,0,18,0,4,
		4,4,4,4,4,4,4,4,4,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,17,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		0,0,0,2,0,3,0,0,0,
		0,1,1,2,0,3,0,3,0,
		0,2,0,0,0,0,0,3,0,
		0,0,0,0,0,0,0,3,1,
		0,0,0,0,0,0,0,0,0,
		1,2,0,0,0,0,0,3,1,
		0,2,0,0,0,0,0,3,0,
		0,1,1,2,0,3,0,1,0,
		0,0,0,2,0,3,0,0,0,
	};
	obj = {
		tobj {
			nam = '';
			x = 0;
			y = 4;
			tile = 30;
			act = teleport;
			obstacle = true;
		},
		tobj {
			nam = '';
			x = 8;
			y = 4;
			tile = 30;
			act = teleport;
			obstacle = true;
		},
		tobj {
			nam = '';
			x = 4;
			y = 8;
			tile = 30;
			act = teleport;
			obstacle = true;
		},
		tobj {
			nam = '';
			x = 4;
			y = 0;
			tile = 30;
			act = teleport;
			obstacle = true;
		},
		tobj {
			nam = 'pad';
			x = 6;
			y = 7;
			tile = 27;
			used = function(s, w)
				if w == rat and not isObstacle(s.x, s.y) then
					drop(rat)
					rat.x = s.x
					rat.y = s.y
					return "-- Siéntate aquí un rato."
				end
			end;
		},
		tobj {
			nam = 'door';
			x = 6;
			y = 1;
			act = function(s)
				if s.obstacle then
					return [[Una puerta de hierro. Está cerrada y no se puede ver el candado.]];
				end
			end;
			var { tile = 5;
			obstacle = true; };
		},
		block(1, 3, 2);
--		block(1, 2, 2);
		block(0, 3, 1);
--		box(0, 3);
		tobj {
			nam = 'out';
			x = 8;
			y = 2;
			tile = 30;
			act = code [[ walk 'map7' ]];
			obstacle = true;
		},
		tobj {
			nam = '';
			x = 5;
			y = 5;
			w = 2;
			h = 2;
			act = function()
				local d = seen 'door'
				if not d.obstacle and not have 'rat' then
					walkin 'stopplz'
					return
				end
			end
		};
	}
}
