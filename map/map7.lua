global { npad = 0 };
local function pad_act(s)
	if s.n == npad + 1 then
		npad = npad + 1
	else
		if s.n == 1 then
			npad = 1
		else
			npad = 0
		end
	end
	if npad == 9 then
		exist 'out'.x = 3;
		exist 'realout':enable();
	else
		exist 'out'.x = 4;
		exist 'realout':disable();
	end
	return true
end

map7 = troom {
	nam = 'quinto piso';
	dsc = [[Un piso más. ¡Ni siquiera hay salida! Solo un patrón aleatorio de losas en el suelo.
	  ¿Tendré que buscar la lógica de nuevo? ¿Ella esta aqui?]];
	dsc2 = [[Parece no haber lógica en este extraño dibujo del suelo.]];
	active = { [[-- ¿Qué es ese pilar en el centro?^ -- Sí, es una estructura extraña.]],
		[[ -- Creo que va a ser más complicado que los esqueletos...^
		-- Al menos nadie nos ataca, así que sientate y piensa...^
		-- Es una pena que no tenga tiempo para esto.^
		-- Lo siento, me olvidé de tu esposa...]],
		[[ -- ¿Cómo se llamaba tu esposa?^
		-- ¿Llamaba?!^
		-- ... ¿cómo se llama tu esposa?^
		-- Arabella...]];
		[[ -- ¡¡¡Dactro, maldito seas!!!^
		-- ...]],
	};
	lifes = { [[-- Estas losas...^
		-- Si, son extrañas.]],
		[[ -- ¿Y si...?^
		-- ???^
		-- Supongo que sólo lo parecen...^]];
		[[ -- Escucha, ¿quizás no hay manera de salir de aquí?^
		-- Para simplificar, supondremos que hay...]];
	};
	left = function(s)
		restore_music();
	end;
	entered = function(s)
		me().x = 7
		me().y = 2
		save_music();
		set_music 'mus/2.ogg'
	end;
	bg = {
		4,4,4,4,4,4,4,4,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,30,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,46,18,18,18,18,18,4,
		4,46,18,18,18,18,18,18,4,
		4,46,46,18,18,18,18,18,4,
		4,4,4,4,4,4,4,4,4,
	};
	wall = {
		1,1,1,1,1,1,1,1,1,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,0,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,33,33,33,33,33,33,33,1,
	};
	obj = {
		tobj {
			nam = 'pad2';
			x = 2;
			y = 2;
			tile = 27;
			n = 2;
			act = pad_act;
		},
		tobj {
			nam = 'pad3';
			x = 6;
			y = 2;
			tile = 27;
			n = 3;
			act = pad_act;
		},
		tobj {
			nam = 'pad4';
			x = 6;
			y = 6;
			tile = 27;
			n = 4;
			act = pad_act;
		},
		tobj {
			nam = 'pad5';
			x = 3;
			y = 6;
			tile = 27;
			n = 5;
			act = pad_act;
		},
		tobj {
			nam = 'pad6';
			x = 3;
			y = 3;
			tile = 27;
			n = 6;
			act = pad_act;
		},
		tobj {
			nam = 'pad7';
			x = 5;
			y = 3;
			tile = 27;
			n = 7;
			act = pad_act;
		},
		tobj {
			nam = 'pad8';
			x = 5;
			y = 5;
			tile = 27;
			n = 8;
			act = pad_act;
		},
		tobj {
			nam = 'pad9';
			x = 4;
			y = 5;
			tile = 27;
			n = 9;
			act = pad_act;
		},
		tobj {
			nam = 'pad1';
			x = 2;
			y = 6;
			tile = 27;
			n = 1;
			act = pad_act;
		},
		tobj {
			nam = 'out';
			var { x = 4; };
			y = 4;
			obstacle = true;
			tile = 32;
		},
		tobj {
			nam = 'realout';
			x = 4;
			y = 4;
			tile = 17;
			act = code [[ walk 'map8' ]];
		}:disable(),
	};
}
