books = function(x, y, txt)
	return tobj {
		nam = 'books';
		obstacle = true;
		var { burning = false; here = false; };
		txt = txt;
		act = function(s)
			if s.burning then
				return [[¡Arden bien!]]
			end
			return stead.call(s, 'txt')
		end;
		x = x;
		y = y;
		tile = 45;
		used = function(s, w)
			if w == fakel then
				if s.burning then
					return "¡Ya están ardiendo!"
				end
				s.burning = rnd(16) + 32
				s.here = here()
				lifeon(s)
				local f = exist('fire', s)
				f:enable()
				return "¡Arded, malditos libros!"
			end
			if w == topor then
				return "Los libros no se pueden destruir con un hacha..."
			end
		end;
		life = function(s)
			if player_moved() then
				if here() ~= s.here then
					lifeoff(s)
				end
				return
			end
			s.burning = s.burning - 1
			if s.burning == 0 then
				lifeoff(s)
				s:disable();
				put (new (string.format("garbage(%d, %d)", s.x, s.y)))
			end
		end;
		obj = {
			tobj {
				nam = 'fire';
				tile = 47;
				x = x;
				y = y;
				float = true;
			}:disable();
		}
	};
end

spider = tobj {
	nam = 'spider';
	float = true;
	var {
		x = 6;
		y = 6;
		lives = 3;
		seen = false;
		obstacle = true;
		panic = 0;
		attack = false;
	};
	tile = 42;
	used = function(s, w)
		local x, y
		s.attack = false
		if w == topor then
			if s.panic > 0 then
				p [[¡Golpeé a la araña con el hacha!]]
				s.lives = s.lives - 1
				if s.lives == 0 then
					p [[¡La bestia está muerta!]]
					lifeoff(s)
					s:disable();
					put(blood)
					blood.x = s.x
					blood.y = s.y
				end
			else
				p [[Agité el hacha, pero la araña era más rápida...]]
				s.attack = true
			end
		elseif w == knife then
			if s.panic > 0 then
				p [[Apuñalé a la araña con mi cuchillo.]]
			else
				p [[Agité el cuchillo, pero la araña era más rápida...]]
				s.attack = true
			end
		elseif w == fakel then
			p [[Agité la antorcha frente a mí y la araña retrocedió.]]
			s.panic = 2
			if s.y == me().y then
				if s.x < me().x then
					x, y = s.x - 1, s.y
				else
					x, y = s.x + 1, s.y
				end
			else -- x
				if s.y < me().y then
					x, y = s.x, s.y - 1
				else
					x, y = s.x, s.y + 1
				end
			end
			if not isObstacle(x, y) then
				s.x, s.y = x, y
			end
		end
	end;
	life = function(s)
		local attack = s.attack
		s.attack = false
		if s.panic > 0 then
			s.panic = s.panic - 1
			return
		end
		if math.abs(me().x - s.x) <= 1 and  math.abs(me().y - s.y) <= 1 then
			s.seen = true;
		end
		if s.seen then
			s.x, s.y = seek(s.x, s.y, me().x, me().y)
		end
		if s.x == me().x and s.y == me().y then
			if type(ACTION_TEXT) ~= 'string' or not attack then
				p "La araña se lanzó sobre mí. Sus patas peludas y frías agarraron firmemente mi cuello. ¡Arabella!"
			else
				p (ACTION_TEXT)
			end
			gameover.tile = 42;
			walkin 'gameover'
		end
	end;
}

stick = tobj {
	tile = 39;
	var { x = 0, y = 0 };
	inv = 'Pata de silla de madera';
	act = function(s) take(s); p "Recogí un trozo de madera del suelo."; end;
	nouse = [[¿Pinchar con un palo?]];
	use = function(s, w)
		if w == hobelen and taken(hobelen) then
			remove(w, me())
			p [[Envolví el tapiz alrededor de un palo.]]
			replace(s, stick_hob, me())
		elseif nameof(w) == 'fireplace' then
			p [[Intenté prender fuego al palo, no funcionó.]]
		end
	end;
	used = function(s, w)
		return stead.call(s, 'use', w);
	end
}:disable();

stick_hob = tobj {
	tile = 40;
	inv = 'Pata de silla de madera envuelta en un tapiz.';
	nouse = [[¿Pinchar con un palo?]];
};

fakel = tobj {
	tile = 41;
	inv = '¡El tapiz arde perfectamente!';
	act = function(s)
		take(s)
		p 'Levanté la antorcha.'
	end;
	var { x = 0, y = 0};
	nouse = [[No tiene sentido quemarlo.]];
};

hobelen = tobj {
	nam = 'hobelen';
	x = 1;
	y = 5;
	tile = 38;
	inv = [[El tapiz muestra una imagen vil]];
	act = [[Un vil tapiz adorna la pared de esta habitación.]];
	used = function(s, w)
		if w == knife then
			if taken(s) then
				p [[¡Rompí el tapiz en pedazos!]]
				remove(s, me())
				return
			end
			p [[Arranqué el tapiz de la pared.]]
			take(s)
			return
		end
	end
}

knife = tobj {
	tile = 44;
	inv = "¡Que agudo!";
	act = function(s) p 'Me llevé el cuchillo.'; take(s) end;
	var { x = 0, y = 0 };
	use = function(s, w)
		if w == rat then
			return "Un chiste malo."
		end
	end;
	nouse = 'Un cuchillo no ayudará aquí.';
}

map8 = troom {
	dsc = [[¡Lo hemos conseguido! El corazón me latía frenéticamente, el pulso me latía en las sienes.
	  ¡Lo hemos conseguido! Miré a mi alrededor.
		Esta vez, la arquitectura me deleitó con la diversidad. Habitaciones y puertas. Bueno, ya veremos...]];
	dsc2 = [[La salida desde este piso está detrás de una puerta de hierro cerrada con llave.]];
	active = {[[-- ¿Cuántos pisos más crees que hay?^
		-- Mi intuición sugiere que hemos recorrido aproximadamente la mitad del camino, pero...^
		-- ¿Pero?^
		-- Pero la geometría dentro de la torre puede distorsionarse...]],
		[[-- ¡Leñador!^
		-- ¿Qué, Rata?^
		-- ¿Cómo te llamas?^
		-- Ron... ¿Cómo te llamas?^
		-- No te lo diré...]],
		[[-- No puedo esperar a salir de aquí...^
		-- Estoy haciéndolo lo mejor que puedo, Rata...]];
	};
	nam = 'sexto piso';
	entered = function(s)
		me().x = 4
		me().y = 4
		key.x = 6
		key.y = 2
		lifeon(spider);
	end;
	left = function(s)
		lifeoff(spider);
	end;
	bg = {
		4,4,4,4,4,4,4,4,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,18,18,18,18,18,18,18,4,
		4,4,4,4,4,4,4,4,4,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,17,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		1,1,1,1,1,1,1,1,1,
		2,0,0,0,0,31,0,0,3,
		35,0,0,32,0,31,0,0,3,
		1,33,33,2,0,3,33,0,1,
		2,0,0,0,0,0,0,0,3,
		1,33,33,35,0,34,33,0,1,
		2,0,0,31,0,31,0,0,3,
		2,0,0,0,0,31,0,0,3,
		1,33,33,33,33,1,33,33,1,
	};
	obj = {
		spider,
		key,
		tobj {
			nam = 'fireplace';
			obstacle = true;
			x = 0;
			y = 1;
			tile = 36;
			act = [[Parece que estuvo encendido recientemente]];
			used = function(s, w)
				if w == stick_hob then
					p [[Prendí fuego a un palo envuelto en un tapiz]]
					replace(stick_hob, fakel, me())
				end
			end;
		},
		stick,
		tobj {
			nam = 'chair';
			x = 1;
			y = 1;
			tile = 37;
			used = function(s, w)
				if w == topor then
					p [[Rompí la silla.]]
					s:disable()
					if disabled 'stick' then
						stick.x = s.x
						stick.y = s.y
						enable(stick);
					end
				end
			end;
		},
		hobelen,
		tobj {
			nam = 'out';
			act = code [[ walk 'map9' ]];
			x = 7;
			y = 7;
			tile = 17;
		},
		openable_door(7, 5);
		win(0, 2);
		win(0, 6);
		win(8, 2);
		wooden_door(3, 1, 3);
		wooden_door(3, 7, 3);
		tobj {
			nam = 'table';
			obstacle = true;
			x = 1;
			y = 2;
			tile = 43;
			used = function(s, w)
				if w == topor then
					p [[Rompí la mesa.]]
					s:disable()
					if disabled 'stick' then
						stick.x = s.x
						stick.y = s.y
						enable(stick);
					end
				end
			end;

			act = function(s)
				if not taken 'knife' then
					take 'knife'
					return "Había un cuchillo sobre la mesa.";
				end
				p "No había nada más sobre la mesa."
			end
		},
		block(7, 3, 33);
		win(8, 6);
		books(2, 6, [[Algunos libros...]])
	};
}
