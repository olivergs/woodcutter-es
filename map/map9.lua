map9 = troom {
	nam = 'septimo piso';
	dsc = [[Subí con la rata al siguiente piso. Unos segundos fueron suficientes para darse cuenta --
	hay acertijos de nuevo ante nosotros. Acertijos que consumen el tiempo, el tiempo de Arabella. ¡Espero que esté viva!]];
	dsc2 = [[Hay un bloque de piedra en el centro de la habitación. La salida al siguiente piso se encuentra en la esquina.]];
	lifes = { [[Ahí está otra vez ese maldito pilar en medio de la habitación.]],
		[[Es extraño que aquí no haya losas móviles en el anterior.]],
		[[¡Este maldito hechicero pagará por todo!!!]]};
	active = { [[-- ¿Por qué no me dices tu nombre?^
		-- Entonces sabrás lo que era antes de ser una rata...]],
		[[-- Tenemos que salir de aquí rápido. Pasamos demasiado tiempo en cada piso...^
		-- Sí, movámonos, Ron.]]};
	entered = function(s)
		me().x = 7;
		me().y = 7;
		lifeon(s);
	end;
	life = function(s)
		local b = exist 'block'
		if here() ~= s then
			return
		end
		if isSomething(3, 3) and isSomething(3, 4) and isSomething(3, 5) and
			isSomething(5, 3) and isSomething(5, 4) and isSomething(5, 5) and
			isSomething(4, 3) and isSomething(4, 5) then
			b.tile = 4
			key.x = 4
			key.y = 4
			if not have(key) and not exist 'door'.opened then
				put(key)
			end
		else
			b.tile = 32
			remove(key)
		end
	end;
	left = function()
		lifeoff(s);
	end;
	bg = {
		0,0,0,0,0,0,0,0,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,18,18,18,18,18,18,18,0,
		0,0,0,0,0,0,0,0,0,
	};
	ground = {
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,46,46,46,0,0,0,
		0,0,0,46,0,46,0,0,0,
		0,0,0,46,46,46,0,0,0,
		0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,17,0,
		0,0,0,0,0,0,0,0,0,
	};
	wall = {
		1,1,1,1,1,1,1,1,1,
		2,0,31,0,0,0,0,0,3,
		1,0,31,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		2,0,0,0,0,0,0,0,3,
		1,33,33,33,33,33,33,33,1,
	};
	obj = {
		openable_door(1, 2);
		tobj {
			nam = 'out';
			x = 1;
			y = 1;
			tile = 17;
			obstacle = true;
			act = function(s)
				if not have 'rat' or not have 'topor' or not have 'knife' or not have 'fakel' then
					return [[Creo que olvidé algo.]]
				end
				walk 'map10'
			end;
		},
		tobj {
			nam = 'block';
			x = 4;
			y = 4;
			var {
				obstacle = true;
				tile = 32;
			}
		},
		win(4, 8);
		win(4, 0);
		win(8, 4);
		win(0, 4);
		box(6, 5);
		box(5, 6);
		box(3, 6);
		tobj {
			nam = 'pad';
			hidden = true;
			x = 3;
			y = 3;
			w = 3;
			h = 3;
			used = function(s, w)
				w.x = ACT_X
				w.y = ACT_Y
				drop(w)
				return true
			end
		},
	};
}
