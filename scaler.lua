game.pic = stead.hook(game.pic, function(f, s, ...)
	local n
	local p
	if here().scaledpic then 
		p = stead.call(here(), 'scaledpic');
	else
		return f(s, ...)
	end
	local op = stead.scaler_pic
	if type(p) == 'string' and p ~= op then
		if op then 
			sprite.free(stead.scaler_sprite)
		end
		n = sprite.load(p)
		if n then
			stead.scaler_sprite = sprite.scale(n, 4.0, 4.0, false)
			sprite.free(n)
			stead.scaler_pic = p
		end
	end
	return stead.scaler_sprite
end)
